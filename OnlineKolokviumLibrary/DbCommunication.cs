﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using OnlineKolokviumLibrary.Models;

namespace OnlineKolokviumLibrary
{
    public class DbCommunication
    {
        public StudentModel PerformLoginStudent(string loginName, string Password)
        {
            StudentModel result = null;
            try
            {
                using (IDbConnection con = new SqlConnection(ProgramHelper.CnnString("OnlineKolokviumDatabase")))
                {
                    var affectedRows = con.Query<StudentModel>("PerformLoginStudents",new { LoginName = loginName, Password = Password }, commandType: CommandType.StoredProcedure).ToList();
                    result = affectedRows.First();
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("The message: " + e.Message);
            }
            return result;
        }

        public ProfesorModel PerformLoginProfesor(string loginName, string password)
        {
            ProfesorModel result = null;
            try
            {
                using (IDbConnection con = new SqlConnection(ProgramHelper.CnnString("OnlineKolokviumDatabase")))
                {
                    var affectedRows = con.Query<ProfesorModel>("PerformLoginProfesors", new { LoginName = loginName, Password = password }, commandType: CommandType.StoredProcedure).ToList();
                    result = affectedRows.First();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The message: " + e.Message);
            }
            return result;
        }

        public TestStatisticsModel GetStatisticsForTest(int id)
        {
            TestStatisticsModel result = new TestStatisticsModel(0, 0, 0);
            try
            {
                using (IDbConnection con = new SqlConnection(ProgramHelper.CnnString("OnlineKolokviumDatabase")))
                {
                    var p = new DynamicParameters();

                    p.Add("@TestID", id);
                    p.Add("@StudentsCount", 0, dbType: DbType.Int32, direction: ParameterDirection.Output);
                    p.Add("@StudentsPassed", 0, dbType: DbType.Int32, direction: ParameterDirection.Output);
                    p.Add("@StudentsFailed", 0, dbType: DbType.Int32, direction: ParameterDirection.Output);
                    con.Execute("GetTestResultStatistics", p, commandType: CommandType.StoredProcedure);
                    result = new TestStatisticsModel(p.Get<int>("@StudentsCount"), p.Get<int>("@StudentsPassed"), p.Get<int>("@StudentsFailed"));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The message GetStatisticsForTest: " + e.Message);
            }
            return result;
        }

        public List<StudentModel> GetStudentsByTakenTest(int id)
        {
            List<StudentModel> result = null;
            try
            {
                using (IDbConnection con = new SqlConnection(ProgramHelper.CnnString("OnlineKolokviumDatabase")))
                {
                    result = con.Query<StudentModel>(@"select S.* from Students as S inner join TestResults as TR on S.ID = TR.StudentID where TR.TestID = @TestID", new { TestID = id }).ToList();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The message GetStudentsByTakenTest: " + e.Message);
            }
            return result;
        }

        public List<TestResultModel> GetTestResultsByTest(int id)
        {
            List<TestResultModel> result = null;
            try
            {
                using (IDbConnection con = new SqlConnection(ProgramHelper.CnnString("OnlineKolokviumDatabase")))
                {
                    result = con.Query<TestResultModel>(@"select * from TestResults where TestID = @TestID", new { TestID = id }).ToList();
                    for (int i = 0; i < result.Count; i++)
                    {
                        var tests = con.Query<TestModel>(@"select * from Tests where ID = @ID", new { ID = result[i].TestID }).ToList();
                        result[i].test = tests.First();

                        var sessions = con.Query<string>(@"select Name from TestSessions where ID = @ID", new { ID = result[i].test.TestSessionID }).ToList();
                        result[i].test.Session = sessions.First();

                        var students = con.Query<StudentModel>(@"select * from Students where ID = @ID", new { ID = result[i].StudentID }).ToList();
                        result[i].student = students.First();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The message GetTestResultsByStudent: " + e.Message);
            }
            return result;
        }

        /// <summary>
        /// Gets the TestResults by Student
        /// Also ensures that TestResultsModel's test will be initialized
        /// and that test's session string will be initialized
        /// </summary>
        /// <param name="id">Studen's ID</param>
        /// <returns>A list of TestResultModel for one student</returns>
        public List<TestResultModel> GetTestResultsByStudent(int id)
        {
            List<TestResultModel> result = null;
            try
            {
                using (IDbConnection con = new SqlConnection(ProgramHelper.CnnString("OnlineKolokviumDatabase")))
                {
                    result = con.Query<TestResultModel>(@"select * from TestResults where StudentID = @StudentID", new { StudentID = id }).ToList();
                    for(int i = 0; i < result.Count; i++)
                    {
                        var tests = con.Query<TestModel>(@"select * from Tests where ID = @ID", new { ID = result[i].TestID }).ToList();
                        result[i].test = tests.First();
                        var sessions = con.Query<string>(@"select Name from TestSessions where ID = @ID", new { ID = result[i].test.TestSessionID }).ToList();
                        result[i].test.Session = sessions.First();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The message GetTestResultsByStudent: " + e.Message);
            }
            return result;
        }

        public List<TestModel> GetAllTests()
        {
            List<TestModel> result = null;
            try
            {
                using (IDbConnection con = new SqlConnection(ProgramHelper.CnnString("OnlineKolokviumDatabase")))
                {
                    result = con.Query<TestModel>(@"select * from Tests").ToList();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The message: " + e.Message);
            }
            return result;
        }

        public List<TestSessionModel> GetAllSessions()
        {
            List<TestSessionModel> result = null;
            try
            {
                using (IDbConnection con = new SqlConnection(ProgramHelper.CnnString("OnlineKolokviumDatabase")))
                {
                    result = con.Query<TestSessionModel>("select * from TestSessions").ToList();
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("The message: " + e.Message);
            }
            return result;
        }

        public List<AnswerModel> GetAnswersByQuestion(int id)
        {
            List<AnswerModel> answers = null;
            try
            {
                using (IDbConnection con = new SqlConnection(ProgramHelper.CnnString("OnlineKolokviumDatabase")))
                {
                    answers = con.Query<AnswerModel>(@"select * from Answers where QuestionID = @QuestionID", new { QuestionID = id }).ToList();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The message: " + e.Message);
            }
            return answers;
        }

        public void CreateResults(TestResultModel results)
        {
            try
            {
                using (IDbConnection con = new SqlConnection(ProgramHelper.CnnString("OnlineKolokviumDatabase")))
                {
                    var p = new DynamicParameters();

                    p.Add("@StudentID", results.StudentID);
                    p.Add("@TestID", results.TestID);
                    p.Add("@IsPassed", results.IsPassed);
                    p.Add("@QuestionsAnswered", results.QuestionsAnswered);
                    p.Add("@QuestionsUnanswered", results.QuestionsUnanswered);
                    p.Add("@CorrectAnswers", results.CorrectAnswers);
                    p.Add("@WrongAnswers", results.WrongAnswers);
                    p.Add("@rid", 0, dbType: DbType.Int32, direction: ParameterDirection.Output);
                    con.Execute("InsertTestResults", p, commandType: CommandType.StoredProcedure);
                    results.ID = p.Get<int>("@rid");

                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The message CreateResults: " + e.Message);
            }
        }

        public List<TestModel> GetTestsBySubjectAndSession(int subjectId, string sessionName)
        {
            List<TestModel> tests = null;
            try
            {
                using (IDbConnection con = new SqlConnection(ProgramHelper.CnnString("OnlineKolokviumDatabase")))
                {
                    tests = con.Query<TestModel>(@"select T.* from Tests as T inner join TestSessions as TS on T.TestSessionID = TS.ID where T.SubjectID = @SubjectID and TS.Name = @Name", new { SubjectID = subjectId, Name = sessionName }).ToList();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The message GetTestsBySubject : " + e.Message);
            }
            return tests;
        }

        public List<SubjectModel> GetSubjectByProfesor(int id)
        {
            List<SubjectModel> subjects = null;
            try
            {
                using (IDbConnection con = new SqlConnection(ProgramHelper.CnnString("OnlineKolokviumDatabase")))
                {
                    subjects = con.Query<SubjectModel>(@"select S.* from Subjects as S inner join Profesors as P on P.ID = S.ProfesorID where S.ProfesorID = @ProfesorID", new { ProfesorID = id }).ToList();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The message: " + e.Message);
            }
            return subjects;
        }

        public void CreateQuestion(QuestionModel question)
        {
            try
            {
                using (IDbConnection con = new SqlConnection(ProgramHelper.CnnString("OnlineKolokviumDatabase")))
                {
                    var p = new DynamicParameters();
                   
                    p.Add("@TestID", question.TestID);
                    p.Add("@Text", question.Text);
                    p.Add("@rid", 0, dbType: DbType.Int32, direction: ParameterDirection.Output);
                    con.Execute("InsertQuestion", p, commandType: CommandType.StoredProcedure);
                    question.ID = p.Get<int>("@rid");

                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The message: " + e.Message);
            }
        }

        public void UpdateQuestion(QuestionModel question)
        {
            try
            {
                using (IDbConnection con = new SqlConnection(ProgramHelper.CnnString("OnlineKolokviumDatabase")))
                {
                    con.Execute(@"update Questions set TestID = @TestID, Text = @Text where ID = @ID", new { TestID = question.TestID, Text = question.Text, ID = question.ID });
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The message: " + e.Message);
            }
        }

        public void DeleteTest(TestModel doomed)
        {
            try
            {
                using (IDbConnection con = new SqlConnection(ProgramHelper.CnnString("OnlineKolokviumDatabase")))
                {
                    con.Execute(@"delete from Tests where ID = @ID", new { ID = doomed.ID });
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The message: " + e.Message);
            }
        }

        public List<string> GetSessions()
        {
            List<string> sessions = null;
            try
            {
                using (IDbConnection con = new SqlConnection(ProgramHelper.CnnString("OnlineKolokviumDatabase")))
                {
                    sessions = con.Query<string>(@"select Name from TestSessions").ToList();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The message: " + e.Message);
            }
            return sessions;
        }

        public void UpdateAnswer(AnswerModel answer)
        {
            try
            {
                using (IDbConnection con = new SqlConnection(ProgramHelper.CnnString("OnlineKolokviumDatabase")))
                {
                    con.Execute(@"update Answers set QuestionID = @QuestionID, Text = @Text, IsCorrect = @IsCorrect where ID = @ID", new { QuestionID = answer.QuestionID, Text = answer.Text, IsCorrect = answer.IsCorrect, ID = answer.ID });
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The message: " + e.Message);
            }
        }

        public void CreateAnswer(AnswerModel answer)
        {
            try
            {
                using (IDbConnection con = new SqlConnection(ProgramHelper.CnnString("OnlineKolokviumDatabase")))
                {
                    var p = new DynamicParameters();

                    p.Add("@QuestionID", answer.QuestionID);
                    p.Add("@Text", answer.Text);
                    p.Add("@IsCorrect", answer.IsCorrect);
                    p.Add("@rid", 0, dbType: DbType.Int32, direction: ParameterDirection.Output);
                    con.Execute("InsertAnswer", p, commandType: CommandType.StoredProcedure);
                    answer.ID = p.Get<int>("@rid");

                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The message: " + e.Message);
            }
        }

        public List<string> resultSetQuery(string sql, CommandType commandType, object param)
        {
            throw new NotImplementedException();
        }

        public int GetAndSetSessionID(string text)
        {
            int result = -1;
            try
            {
                using (IDbConnection con = new SqlConnection(ProgramHelper.CnnString("OnlineKolokviumDatabase")))
                {
                    var s = con.Query<int>(@"select ID from TestSessions where Name = @Text", new { Text = text }).ToList();
                    if (s.Count > 0)
                    {
                        result = s[0];
                    }
                    else
                    {
                        var p = new DynamicParameters();
                        p.Add("@Name", text);
                        p.Add("@id", 0, dbType: DbType.Int32, direction: ParameterDirection.Output);
                        con.Execute("InsertSession", p, commandType: CommandType.StoredProcedure);
                        result = p.Get<int>("@id");
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The message: " + e.Message);
            }
            return result;
        }

        public List<QuestionModel> GetQuestionsByTest(int id)
        {
            List<QuestionModel> questions = null;
            try
            {
                using (IDbConnection con = new SqlConnection(ProgramHelper.CnnString("OnlineKolokviumDatabase")))
                {
                    questions = con.Query<QuestionModel>(@"select * from Questions where TestID = @TestID", new { TestID = id }).ToList();
                    for(int i = 0; i < questions.Count; i++)
                    {
                        questions[i].Answers = this.GetAnswersByQuestion(questions[i].ID);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The message: " + e.Message);
            }
            return questions;
        }

        public void CreateTest(TestModel test)
        {
            try
            {
                using (IDbConnection con = new SqlConnection(ProgramHelper.CnnString("OnlineKolokviumDatabase")))
                {
                    var p = new DynamicParameters();
                    p.Add("@ID", test.ID);
                    p.Add("@Name", test.Name);
                    p.Add("@SubjectID", test.SubjectID);
                    p.Add("@Date", test.Date);
                    p.Add("@TestSessionID", test.TestSessionID);
                    p.Add("@IsActive", test.IsActive);
                    p.Add("@Duration", test.Duration);
                    p.Add("@rid", 0, dbType: DbType.Int32, direction: ParameterDirection.Output);
                    con.Execute("InsertTest", p, commandType: CommandType.StoredProcedure);
                    test.ID = p.Get<int>("@rid");

                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The message: " + e.Message);
            }
        }

        public void DeleteQuestion(QuestionModel doomed)
        {
            try
            {
                using (IDbConnection con = new SqlConnection(ProgramHelper.CnnString("OnlineKolokviumDatabase")))
                {
                    con.Execute(@"delete from Questions where ID = @ID", new { ID = doomed.ID });
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The message: " + e.Message);
            }
        }

        public void UpdateTest(TestModel test)
        {
            try
            {
                using (IDbConnection con = new SqlConnection(ProgramHelper.CnnString("OnlineKolokviumDatabase")))
                {
                    con.Execute(@"update Tests set Name = @Name, SubjectID = @SubjectID, Date = @Date, TestSessionID = @TestSessionID, IsActive = @IsActive, Duration = @Duration where ID = @ID", new { Name = test.Name, SubjectID = test.SubjectID, Date = test.Date, TestSessionID = test.TestSessionID, IsActive = test.IsActive, Duration = test.Duration, ID = test.ID });
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The message: " + e.Message);
            }
        }
    }
}
