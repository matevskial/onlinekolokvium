﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineKolokviumLibrary
{
    public class CountDown
    {
        Timer timer;
        private int mins;
        private int secs;

        public int Minutes { get; set; }
        public int Seconds { get; set; }
        public int TotalSeconds { get; set; }
        public int Ticks { get; set; }

        public CountDown(int minutes, int seconds, EventHandler elapsed)
        {
            timer = new Timer();
            timer.Interval = 1000; // 1 second

            timer.Tick += elapsed;
            mins = minutes;
            secs = seconds;
            Minutes = minutes;
            Seconds = seconds;
            TotalSeconds = Minutes * 60 + Seconds;
        }

        public void Start()
        {
            Minutes = mins;
            Seconds = secs;
            timer.Start();
        }

        public void Tick()
        {
            Seconds = Math.Max(0, Seconds - 1);
            if(Seconds == 0)
            {
                Minutes = Math.Max(0, Minutes - 1);
                Seconds = 59;
            }
            Ticks++;
        }

        public string Display()
        {
            return string.Format("{0}:{1}", Minutes.ToString().PadLeft(2, '0'), Seconds.ToString().PadLeft(2,'0') );
        }

        public void Stop()
        {
            timer.Stop();
        }
    }
}
