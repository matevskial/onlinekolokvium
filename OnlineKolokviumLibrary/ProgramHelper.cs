﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineKolokviumLibrary
{
    /// <summary>
    /// A class that has some static helper methods that are global for the
    /// solution
    /// </summary>
    public class ProgramHelper
    {
        public static DbCommunication Db { get; set; }

        public static void Initialize()
        {
            Db = new DbCommunication();
        }

        public static string CnnString(string name)
        {
            return ConfigurationManager.ConnectionStrings[name].ConnectionString;
        }

        public static string GenerateSessionString(DateTime d)
        {
            if (d.Month == 1)
                return "јануарска " + d.Year;
            else if (d.Month == 2)
                return "февруарска " + d.Year;
            else if (d.Month == 4)
                return "априлска " + d.Year;
            else if (d.Month == 6)
                return "јунска " + d.Year;
            else if (d.Month == 9)
                return "септемвриска " + d.Year;
            else if (d.Month == 11)
                return "ноемвриска " + d.Year;
            else
                return "";
        }
    }
}
