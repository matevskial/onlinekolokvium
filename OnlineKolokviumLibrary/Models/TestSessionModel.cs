﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineKolokviumLibrary.Models
{
    public class TestSessionModel
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public TestSessionModel(int iD, string name)
        {
            ID = iD;
            Name = name;
        }
    }
}
