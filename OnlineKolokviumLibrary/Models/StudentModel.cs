﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineKolokviumLibrary.Models
{
    public class StudentModel
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Index { get; set; }

        /*
        public StudentModel()
        {

        }
        */
        public StudentModel(int id, string firstName, string lastName, string index)
        {
            ID = id;
            FirstName = firstName;
            LastName = lastName;
            Index = index;
        }
    }
}
