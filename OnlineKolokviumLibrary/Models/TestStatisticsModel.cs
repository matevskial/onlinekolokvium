﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineKolokviumLibrary.Models
{
    public class TestStatisticsModel
    {
        public int StudentsCount { get; set; }
        public int StudentsPassed { get; set; }
        public int StudentsFailed { get; set; }

        public TestStatisticsModel(int studentsCount, int studentsPassed, int studentsFailed)
        {
            StudentsCount = studentsCount;
            StudentsPassed = studentsPassed;
            StudentsFailed = studentsFailed;
        }
    }
}
