﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineKolokviumLibrary.Models
{
    public class QuestionModel
    {
        public int ID { get; set; }
        public int TestID { get; set; }
        public string Text { get; set; }

        public List<AnswerModel> Answers { get; set; }
        public TestModel Test { get; set; }

        public QuestionModel(int id, int testID, string text)
        {
            this.ID = id;
            TestID = testID;
            this.Text = text;
            Answers = new List<AnswerModel>();
        }
    }
}
