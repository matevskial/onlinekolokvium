﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineKolokviumLibrary.Models
{
    public class AnswerModel
    {
        public int ID { get; set; }
        public int QuestionID { get; set; }
        public string Text { get; set; }
        public bool IsCorrect { get; set; }

        public AnswerModel(int id, int questionID, string text, bool isCorrect)
        {
            this.ID = id;
            this.QuestionID = questionID;
            this.Text = text;
            this.IsCorrect = isCorrect;
        }

    }
}
