﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineKolokviumLibrary.Models
{
    public class TestResultModel
    {
        public int ID { get; set; }
        public int StudentID { get; set; }
        public int TestID { get; set; }
        public bool IsPassed { get; set; }
        public int QuestionsAnswered { get; set; }
        public int QuestionsUnanswered { get; set; }
        public int CorrectAnswers { get; set; }
        public int WrongAnswers { get; set; }

        public TestModel test;
        public StudentModel student;

        public TestResultModel(int iD, int studentID, int testID, bool isPassed, int questionsAnswered, int questionsUnanswered, int correctAnswers, int wrongAnswers)
        {
            ID = iD;
            StudentID = studentID;
            TestID = testID;
            IsPassed = isPassed;
            QuestionsAnswered = questionsAnswered;
            QuestionsUnanswered = questionsUnanswered;
            CorrectAnswers = correctAnswers;
            WrongAnswers = wrongAnswers;
        }

        


    }
}
