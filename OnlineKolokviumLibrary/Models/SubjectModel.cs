﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineKolokviumLibrary.Models
{
    public class SubjectModel
    {
        public int ID { get; set; }
        public int ProfesorID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }

        public SubjectModel(int id, int profesorID, string name, string code)
        {
            ID = id;
            ProfesorID = profesorID;
            Name = name;
            Code = code;
        }
    }
}
