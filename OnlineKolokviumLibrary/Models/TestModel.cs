﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineKolokviumLibrary.Models
{
    public class TestModel
    {
        // Properties that ORM will use when querying select
        public int ID { get; set; }
        public string Name { get; set; }
        public int SubjectID { get; set; }
        public DateTime Date { get; set; }
        public int TestSessionID { get; set; }
        public bool IsActive { get; set; }
        public int Duration { get; set; }

        // Properties that program will use
        public string Session { get; set; }
        public List<QuestionModel> Questions { get; set; }

        public TestModel(int id, string name, int subjectID, DateTime date, int testSessionID, bool isActive, int duration)
        {
            ID = id;
            Name = name;
            SubjectID = subjectID;
            Date = date;
            TestSessionID = testSessionID;
            IsActive = isActive;
            Duration = duration;
        }
    }
}
