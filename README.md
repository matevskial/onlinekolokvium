# OnlineKolokvium

A C# WinForms application where students can take tests and see results, and profesors
can create tests for students to take.

This is a project for the course Visual Programming in my uni.

* Dashboard for logged student

![Screenshot 1](media/OnlineKolokvium-student-dashboard.png "Dashboard for logged student")

* Form for taking the test

![Screenshot 2](media/OnlineKolokvium-student-take-test.png "Form for taking the test")

* Dashboard for logged profesor

![Screenshot 3](media/OnlineKolokvium-profesor-dashboard.png "Dashboard for logged profesor")

* Form for creating a test

![Screensot 4](media/OnlineKolokvium-profesor-create-test.png "Form for creating a test")
