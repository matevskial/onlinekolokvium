﻿namespace OnlineKolokviumUI
{
    partial class FormProfesorLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMessageLoginPassword = new System.Windows.Forms.Label();
            this.lblMessageLoginId = new System.Windows.Forms.Label();
            this.txtBoxLoginId = new System.Windows.Forms.TextBox();
            this.btnLogin = new System.Windows.Forms.Button();
            this.txtBoxLoginPassword = new System.Windows.Forms.TextBox();
            this.mnuStrip = new System.Windows.Forms.MenuStrip();
            this.mnuStripHome = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblMessageLoginPassword
            // 
            this.lblMessageLoginPassword.AutoSize = true;
            this.lblMessageLoginPassword.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblMessageLoginPassword.Location = new System.Drawing.Point(135, 137);
            this.lblMessageLoginPassword.Name = "lblMessageLoginPassword";
            this.lblMessageLoginPassword.Size = new System.Drawing.Size(93, 30);
            this.lblMessageLoginPassword.TabIndex = 10;
            this.lblMessageLoginPassword.Text = "Лозинка";
            // 
            // lblMessageLoginId
            // 
            this.lblMessageLoginId.AutoSize = true;
            this.lblMessageLoginId.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblMessageLoginId.Location = new System.Drawing.Point(135, 62);
            this.lblMessageLoginId.Name = "lblMessageLoginId";
            this.lblMessageLoginId.Size = new System.Drawing.Size(173, 30);
            this.lblMessageLoginId.TabIndex = 9;
            this.lblMessageLoginId.Text = "Корисничко име";
            // 
            // txtBoxLoginId
            // 
            this.txtBoxLoginId.Location = new System.Drawing.Point(140, 95);
            this.txtBoxLoginId.Name = "txtBoxLoginId";
            this.txtBoxLoginId.Size = new System.Drawing.Size(210, 35);
            this.txtBoxLoginId.TabIndex = 7;
            // 
            // btnLogin
            // 
            this.btnLogin.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogin.ForeColor = System.Drawing.SystemColors.GrayText;
            this.btnLogin.Location = new System.Drawing.Point(161, 260);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(150, 40);
            this.btnLogin.TabIndex = 6;
            this.btnLogin.Text = "Најави се";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // txtBoxLoginPassword
            // 
            this.txtBoxLoginPassword.Location = new System.Drawing.Point(140, 170);
            this.txtBoxLoginPassword.Name = "txtBoxLoginPassword";
            this.txtBoxLoginPassword.PasswordChar = '*';
            this.txtBoxLoginPassword.Size = new System.Drawing.Size(210, 35);
            this.txtBoxLoginPassword.TabIndex = 11;
            // 
            // mnuStrip
            // 
            this.mnuStrip.BackColor = System.Drawing.Color.Lavender;
            this.mnuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuStripHome});
            this.mnuStrip.Location = new System.Drawing.Point(0, 0);
            this.mnuStrip.Name = "mnuStrip";
            this.mnuStrip.Size = new System.Drawing.Size(484, 24);
            this.mnuStrip.TabIndex = 12;
            this.mnuStrip.Text = "menuStrip1";
            // 
            // mnuStripHome
            // 
            this.mnuStripHome.Name = "mnuStripHome";
            this.mnuStripHome.Size = new System.Drawing.Size(66, 20);
            this.mnuStripHome.Text = "Почеток";
            this.mnuStripHome.Click += new System.EventHandler(this.mnuStripHome_Click);
            // 
            // FormProfesorLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.ClientSize = new System.Drawing.Size(484, 362);
            this.Controls.Add(this.txtBoxLoginPassword);
            this.Controls.Add(this.lblMessageLoginPassword);
            this.Controls.Add(this.lblMessageLoginId);
            this.Controls.Add(this.txtBoxLoginId);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.mnuStrip);
            this.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.GrayText;
            this.MainMenuStrip = this.mnuStrip;
            this.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.Name = "FormProfesorLogin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OnlineKolokvium - Најави се - Професор";
            this.mnuStrip.ResumeLayout(false);
            this.mnuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMessageLoginPassword;
        private System.Windows.Forms.Label lblMessageLoginId;
        private System.Windows.Forms.TextBox txtBoxLoginId;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.TextBox txtBoxLoginPassword;
        private System.Windows.Forms.MenuStrip mnuStrip;
        private System.Windows.Forms.ToolStripMenuItem mnuStripHome;
    }
}