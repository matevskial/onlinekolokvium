﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OnlineKolokviumLibrary;
using OnlineKolokviumLibrary.Models;

namespace OnlineKolokviumUI
{
    public partial class FormStudentTestInitialSplash : Form
    {
        TestModel test;

        public FormStudentTestInitialSplash()
        {
            InitializeComponent();
        }

        public FormStudentTestInitialSplash(TestModel test)
        {
            InitializeComponent();
            this.test = test;
        }

        private void FormStudentTestInitialSplash_Load(object sender, EventArgs e)
        {
            lblMessageRules.Text = string.Format(lblMessageRules.Text, test.Questions.Count, test.Duration);
        }

        private void btnStartTest_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancelTest_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
