﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OnlineKolokviumLibrary;
using OnlineKolokviumLibrary.Models;

namespace OnlineKolokviumUI
{
    public partial class FormProfesorDashboard : Form
    {
        FormMain frmMain;
        ProfesorModel p;

        List<SubjectModel> subjects;
        List<string> sessions;
        List<TestModel> tests;

        public FormProfesorDashboard(FormMain frmMain, ProfesorModel p)
        {
            InitializeComponent();
            this.frmMain = frmMain;
            this.p = p;
        }

        private void FormProfesorDashboard_Load(object sender, EventArgs e)
        {
            lblProfesorInfo.Text = p.FirstName + " "  + p.LastName;

            FillData();
        }

        private void mnuStripLogout_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Дали сте сигурни дека сакате да се одјавите?", "Потврда", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                frmMain.Show();
                this.Close();
            }
        }

        private void FillData()
        {
            subjects = ProgramHelper.Db.GetSubjectByProfesor(p.ID);
            cmbBoxSubjects.DataSource = subjects;
            cmbBoxSubjects.DisplayMember = "Name";

            sessions = ProgramHelper.Db.GetSessions();
            cmbBoxSessions.DataSource = sessions;

            
            FillLstBoxTests();
        }

        private void FillLstBoxTests()
        {
            int subjectID = ((SubjectModel)cmbBoxSubjects.SelectedItem).ID;
            string sessionName = ((string)cmbBoxSessions.SelectedItem);
            tests = ProgramHelper.Db.GetTestsBySubjectAndSession(subjectID, sessionName);
            lstBoxTests.DataSource = tests;
            lstBoxTests.DisplayMember = "Name";
            lstBoxTests.ClearSelected();
        }

        private void btnNewTest_Click(object sender, EventArgs e)
        {
            SubjectModel subject = ((SubjectModel)cmbBoxSubjects.SelectedItem);
            FormProfesorCreateEditTest frmProfesorCreateEditTest = new FormProfesorCreateEditTest(subjects, subject);
            if(frmProfesorCreateEditTest.ShowDialog() == DialogResult.OK)
            {
                FillData();
            }
            //Console.WriteLine(frmProfesorCreateEditTest.ShowDialog());
        }

        private void btnEditTest_Click(object sender, EventArgs e)
        {
            TestModel test = ((TestModel)lstBoxTests.SelectedItem);
            if (test == null) return;
            SubjectModel subject = ((SubjectModel)cmbBoxSubjects.SelectedItem);
            FormProfesorCreateEditTest frmProfesorCreateEditTest = new FormProfesorCreateEditTest(subjects, test, subject);
            if (frmProfesorCreateEditTest.ShowDialog() == DialogResult.OK)
            {
                FillData();
            }
            //Console.WriteLine(frmProfesorCreateEditTest.ShowDialog());
        }

        private void btnDeleteTest_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Дали сте сигурни дека сакате да го избришите тестот?", "Потврда", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {
                
                TestModel doomed = ((TestModel)lstBoxTests.SelectedItem);
               
                ProgramHelper.Db.DeleteTest(doomed);
                FillLstBoxTests();
            }

        }

        private void cmbBoxSubjects_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillLstBoxTests();
        }

        private void cmbBoxSessions_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillLstBoxTests();
        }

        private void lstBoxTests_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstBoxTests.SelectedIndex >= 0)
            {
                btnDeleteTest.Enabled = true;
                btnEditTest.Enabled = true;
                btnViewTest.Enabled = true;
            }
            else
            {
                btnDeleteTest.Enabled = false;
                btnEditTest.Enabled = false;
                btnViewTest.Enabled = false;
            }
        }

        private void btnViewTest_Click(object sender, EventArgs e)
        {
            TestModel t = ((TestModel)lstBoxTests.SelectedItem);
            FormProfesorTestStatistics frmProfesorTestStatistics = new FormProfesorTestStatistics(t);
            frmProfesorTestStatistics.Show();
        }
    }
}
