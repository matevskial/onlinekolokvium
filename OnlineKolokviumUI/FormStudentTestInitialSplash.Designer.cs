﻿namespace OnlineKolokviumUI
{
    partial class FormStudentTestInitialSplash
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormStudentTestInitialSplash));
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.btnStartTest = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lblMessageRules = new System.Windows.Forms.Label();
            this.btnCancelTest = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnStartTest
            // 
            this.btnStartTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStartTest.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartTest.ForeColor = System.Drawing.SystemColors.GrayText;
            this.btnStartTest.Location = new System.Drawing.Point(94, 268);
            this.btnStartTest.Name = "btnStartTest";
            this.btnStartTest.Size = new System.Drawing.Size(193, 40);
            this.btnStartTest.TabIndex = 11;
            this.btnStartTest.Text = "Полагај";
            this.btnStartTest.UseVisualStyleBackColor = true;
            this.btnStartTest.Click += new System.EventHandler(this.btnStartTest_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(184, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(221, 30);
            this.label1.TabIndex = 12;
            this.label1.Text = "Правила за полагање";
            // 
            // lblMessageRules
            // 
            this.lblMessageRules.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessageRules.Location = new System.Drawing.Point(12, 68);
            this.lblMessageRules.Name = "lblMessageRules";
            this.lblMessageRules.Size = new System.Drawing.Size(565, 185);
            this.lblMessageRules.TabIndex = 13;
            this.lblMessageRules.Text = resources.GetString("lblMessageRules.Text");
            // 
            // btnCancelTest
            // 
            this.btnCancelTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelTest.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelTest.ForeColor = System.Drawing.SystemColors.GrayText;
            this.btnCancelTest.Location = new System.Drawing.Point(293, 268);
            this.btnCancelTest.Name = "btnCancelTest";
            this.btnCancelTest.Size = new System.Drawing.Size(193, 40);
            this.btnCancelTest.TabIndex = 14;
            this.btnCancelTest.Text = "Откажи";
            this.btnCancelTest.UseVisualStyleBackColor = true;
            this.btnCancelTest.Click += new System.EventHandler(this.btnCancelTest_Click);
            // 
            // FormStudentTestInitialSplash
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.ClientSize = new System.Drawing.Size(589, 320);
            this.Controls.Add(this.btnCancelTest);
            this.Controls.Add(this.lblMessageRules);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnStartTest);
            this.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.GrayText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.Name = "FormStudentTestInitialSplash";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OnlineKolovkium - Студент - Правила на полагање";
            this.Load += new System.EventHandler(this.FormStudentTestInitialSplash_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button btnStartTest;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblMessageRules;
        private System.Windows.Forms.Button btnCancelTest;
    }
}