﻿namespace OnlineKolokviumUI
{
    partial class FormProfesorDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mnuStrip = new System.Windows.Forms.MenuStrip();
            this.mnuStripLogout = new System.Windows.Forms.ToolStripMenuItem();
            this.btnNewTest = new System.Windows.Forms.Button();
            this.cmbBoxSubjects = new System.Windows.Forms.ComboBox();
            this.lstBoxTests = new System.Windows.Forms.ListBox();
            this.btnViewTest = new System.Windows.Forms.Button();
            this.btnEditTest = new System.Windows.Forms.Button();
            this.lblMessageSubject = new System.Windows.Forms.Label();
            this.cmbBoxSessions = new System.Windows.Forms.ComboBox();
            this.lblMessageSession = new System.Windows.Forms.Label();
            this.lblProfesorInfo = new System.Windows.Forms.Label();
            this.btnDeleteTest = new System.Windows.Forms.Button();
            this.mnuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnuStrip
            // 
            this.mnuStrip.BackColor = System.Drawing.Color.Lavender;
            this.mnuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuStripLogout});
            this.mnuStrip.Location = new System.Drawing.Point(0, 0);
            this.mnuStrip.Name = "mnuStrip";
            this.mnuStrip.Size = new System.Drawing.Size(544, 24);
            this.mnuStrip.TabIndex = 14;
            this.mnuStrip.Text = "menuStrip1";
            // 
            // mnuStripLogout
            // 
            this.mnuStripLogout.Name = "mnuStripLogout";
            this.mnuStripLogout.Size = new System.Drawing.Size(71, 20);
            this.mnuStripLogout.Text = "Одјави се";
            this.mnuStripLogout.Click += new System.EventHandler(this.mnuStripLogout_Click);
            // 
            // btnNewTest
            // 
            this.btnNewTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewTest.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewTest.ForeColor = System.Drawing.SystemColors.GrayText;
            this.btnNewTest.Location = new System.Drawing.Point(389, 363);
            this.btnNewTest.Name = "btnNewTest";
            this.btnNewTest.Size = new System.Drawing.Size(115, 40);
            this.btnNewTest.TabIndex = 19;
            this.btnNewTest.Text = "Нов тест";
            this.btnNewTest.UseVisualStyleBackColor = true;
            this.btnNewTest.Click += new System.EventHandler(this.btnNewTest_Click);
            // 
            // cmbBoxSubjects
            // 
            this.cmbBoxSubjects.BackColor = System.Drawing.SystemColors.Window;
            this.cmbBoxSubjects.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoxSubjects.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBoxSubjects.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbBoxSubjects.FormattingEnabled = true;
            this.cmbBoxSubjects.Location = new System.Drawing.Point(23, 80);
            this.cmbBoxSubjects.Name = "cmbBoxSubjects";
            this.cmbBoxSubjects.Size = new System.Drawing.Size(239, 29);
            this.cmbBoxSubjects.TabIndex = 18;
            this.cmbBoxSubjects.SelectedIndexChanged += new System.EventHandler(this.cmbBoxSubjects_SelectedIndexChanged);
            // 
            // lstBoxTests
            // 
            this.lstBoxTests.BackColor = System.Drawing.Color.LavenderBlush;
            this.lstBoxTests.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstBoxTests.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lstBoxTests.FormattingEnabled = true;
            this.lstBoxTests.ItemHeight = 21;
            this.lstBoxTests.Location = new System.Drawing.Point(23, 138);
            this.lstBoxTests.Name = "lstBoxTests";
            this.lstBoxTests.Size = new System.Drawing.Size(504, 193);
            this.lstBoxTests.TabIndex = 17;
            this.lstBoxTests.SelectedIndexChanged += new System.EventHandler(this.lstBoxTests_SelectedIndexChanged);
            // 
            // btnViewTest
            // 
            this.btnViewTest.Enabled = false;
            this.btnViewTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnViewTest.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViewTest.ForeColor = System.Drawing.SystemColors.GrayText;
            this.btnViewTest.Location = new System.Drawing.Point(26, 363);
            this.btnViewTest.Name = "btnViewTest";
            this.btnViewTest.Size = new System.Drawing.Size(115, 40);
            this.btnViewTest.TabIndex = 16;
            this.btnViewTest.Text = "Преглед";
            this.btnViewTest.UseVisualStyleBackColor = true;
            this.btnViewTest.Click += new System.EventHandler(this.btnViewTest_Click);
            // 
            // btnEditTest
            // 
            this.btnEditTest.Enabled = false;
            this.btnEditTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditTest.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditTest.ForeColor = System.Drawing.SystemColors.GrayText;
            this.btnEditTest.Location = new System.Drawing.Point(268, 363);
            this.btnEditTest.Name = "btnEditTest";
            this.btnEditTest.Size = new System.Drawing.Size(115, 40);
            this.btnEditTest.TabIndex = 20;
            this.btnEditTest.Text = "Уреди";
            this.btnEditTest.UseVisualStyleBackColor = true;
            this.btnEditTest.Click += new System.EventHandler(this.btnEditTest_Click);
            // 
            // lblMessageSubject
            // 
            this.lblMessageSubject.AutoSize = true;
            this.lblMessageSubject.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessageSubject.Location = new System.Drawing.Point(19, 56);
            this.lblMessageSubject.Name = "lblMessageSubject";
            this.lblMessageSubject.Size = new System.Drawing.Size(73, 21);
            this.lblMessageSubject.TabIndex = 21;
            this.lblMessageSubject.Text = "Предмет";
            // 
            // cmbBoxSessions
            // 
            this.cmbBoxSessions.BackColor = System.Drawing.SystemColors.Window;
            this.cmbBoxSessions.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoxSessions.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBoxSessions.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbBoxSessions.FormattingEnabled = true;
            this.cmbBoxSessions.Location = new System.Drawing.Point(288, 80);
            this.cmbBoxSessions.Name = "cmbBoxSessions";
            this.cmbBoxSessions.Size = new System.Drawing.Size(239, 29);
            this.cmbBoxSessions.TabIndex = 22;
            this.cmbBoxSessions.SelectedIndexChanged += new System.EventHandler(this.cmbBoxSessions_SelectedIndexChanged);
            // 
            // lblMessageSession
            // 
            this.lblMessageSession.AutoSize = true;
            this.lblMessageSession.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessageSession.Location = new System.Drawing.Point(284, 56);
            this.lblMessageSession.Name = "lblMessageSession";
            this.lblMessageSession.Size = new System.Drawing.Size(56, 21);
            this.lblMessageSession.TabIndex = 23;
            this.lblMessageSession.Text = "Сесија";
            // 
            // lblProfesorInfo
            // 
            this.lblProfesorInfo.AutoSize = true;
            this.lblProfesorInfo.Location = new System.Drawing.Point(18, 26);
            this.lblProfesorInfo.Name = "lblProfesorInfo";
            this.lblProfesorInfo.Size = new System.Drawing.Size(155, 30);
            this.lblProfesorInfo.TabIndex = 24;
            this.lblProfesorInfo.Text = "<ProfesorInfo>";
            // 
            // btnDeleteTest
            // 
            this.btnDeleteTest.Enabled = false;
            this.btnDeleteTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteTest.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteTest.ForeColor = System.Drawing.SystemColors.GrayText;
            this.btnDeleteTest.Location = new System.Drawing.Point(147, 363);
            this.btnDeleteTest.Name = "btnDeleteTest";
            this.btnDeleteTest.Size = new System.Drawing.Size(115, 40);
            this.btnDeleteTest.TabIndex = 25;
            this.btnDeleteTest.Text = "Избриши";
            this.btnDeleteTest.UseVisualStyleBackColor = false;
            this.btnDeleteTest.Click += new System.EventHandler(this.btnDeleteTest_Click);
            // 
            // FormProfesorDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.ClientSize = new System.Drawing.Size(544, 422);
            this.Controls.Add(this.btnDeleteTest);
            this.Controls.Add(this.lblProfesorInfo);
            this.Controls.Add(this.lblMessageSession);
            this.Controls.Add(this.cmbBoxSessions);
            this.Controls.Add(this.lblMessageSubject);
            this.Controls.Add(this.btnEditTest);
            this.Controls.Add(this.btnNewTest);
            this.Controls.Add(this.cmbBoxSubjects);
            this.Controls.Add(this.lstBoxTests);
            this.Controls.Add(this.btnViewTest);
            this.Controls.Add(this.mnuStrip);
            this.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.GrayText;
            this.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.Name = "FormProfesorDashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OnlineKolovkium - Професор - Почетна";
            this.Load += new System.EventHandler(this.FormProfesorDashboard_Load);
            this.mnuStrip.ResumeLayout(false);
            this.mnuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mnuStrip;
        private System.Windows.Forms.ToolStripMenuItem mnuStripLogout;
        private System.Windows.Forms.Button btnNewTest;
        private System.Windows.Forms.ComboBox cmbBoxSubjects;
        private System.Windows.Forms.ListBox lstBoxTests;
        private System.Windows.Forms.Button btnViewTest;
        private System.Windows.Forms.Button btnEditTest;
        private System.Windows.Forms.Label lblMessageSubject;
        private System.Windows.Forms.ComboBox cmbBoxSessions;
        private System.Windows.Forms.Label lblMessageSession;
        private System.Windows.Forms.Label lblProfesorInfo;
        private System.Windows.Forms.Button btnDeleteTest;
    }
}