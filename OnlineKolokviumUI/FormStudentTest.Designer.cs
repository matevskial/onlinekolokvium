﻿namespace OnlineKolokviumUI
{
    partial class FormStudentTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblTimer = new System.Windows.Forms.Label();
            this.lblTestName = new System.Windows.Forms.Label();
            this.grpBoxQuestion = new System.Windows.Forms.GroupBox();
            this.flwLayoutAnswers = new System.Windows.Forms.FlowLayoutPanel();
            this.txtBoxQuestion = new System.Windows.Forms.TextBox();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPrevious = new System.Windows.Forms.Button();
            this.btnEnd = new System.Windows.Forms.Button();
            this.lblStudentInfo = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lstViewQuestions = new System.Windows.Forms.ListView();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.grpBoxQuestion.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTimer
            // 
            this.lblTimer.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.lblTimer.Location = new System.Drawing.Point(380, 6);
            this.lblTimer.Name = "lblTimer";
            this.lblTimer.Size = new System.Drawing.Size(89, 21);
            this.lblTimer.TabIndex = 1;
            this.lblTimer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTestName
            // 
            this.lblTestName.AutoSize = true;
            this.lblTestName.Location = new System.Drawing.Point(3, 6);
            this.lblTestName.Name = "lblTestName";
            this.lblTestName.Size = new System.Drawing.Size(0, 21);
            this.lblTestName.TabIndex = 0;
            // 
            // grpBoxQuestion
            // 
            this.grpBoxQuestion.Controls.Add(this.flwLayoutAnswers);
            this.grpBoxQuestion.Controls.Add(this.txtBoxQuestion);
            this.grpBoxQuestion.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBoxQuestion.Location = new System.Drawing.Point(110, 103);
            this.grpBoxQuestion.Name = "grpBoxQuestion";
            this.grpBoxQuestion.Size = new System.Drawing.Size(462, 301);
            this.grpBoxQuestion.TabIndex = 2;
            this.grpBoxQuestion.TabStop = false;
            this.grpBoxQuestion.Text = "Прашање - ";
            // 
            // flwLayoutAnswers
            // 
            this.flwLayoutAnswers.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flwLayoutAnswers.Location = new System.Drawing.Point(7, 158);
            this.flwLayoutAnswers.Name = "flwLayoutAnswers";
            this.flwLayoutAnswers.Size = new System.Drawing.Size(449, 137);
            this.flwLayoutAnswers.TabIndex = 1;
            // 
            // txtBoxQuestion
            // 
            this.txtBoxQuestion.BackColor = System.Drawing.Color.LavenderBlush;
            this.txtBoxQuestion.Enabled = false;
            this.txtBoxQuestion.Location = new System.Drawing.Point(7, 29);
            this.txtBoxQuestion.Multiline = true;
            this.txtBoxQuestion.Name = "txtBoxQuestion";
            this.txtBoxQuestion.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtBoxQuestion.Size = new System.Drawing.Size(449, 123);
            this.txtBoxQuestion.TabIndex = 0;
            // 
            // btnNext
            // 
            this.btnNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNext.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.ForeColor = System.Drawing.SystemColors.GrayText;
            this.btnNext.Location = new System.Drawing.Point(339, 410);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(193, 40);
            this.btnNext.TabIndex = 10;
            this.btnNext.Text = "Следно";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPrevious
            // 
            this.btnPrevious.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrevious.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrevious.ForeColor = System.Drawing.SystemColors.GrayText;
            this.btnPrevious.Location = new System.Drawing.Point(110, 410);
            this.btnPrevious.Name = "btnPrevious";
            this.btnPrevious.Size = new System.Drawing.Size(193, 40);
            this.btnPrevious.TabIndex = 11;
            this.btnPrevious.Text = "Претходно";
            this.btnPrevious.UseVisualStyleBackColor = true;
            this.btnPrevious.Click += new System.EventHandler(this.btnPrevious_Click);
            // 
            // btnEnd
            // 
            this.btnEnd.BackColor = System.Drawing.Color.IndianRed;
            this.btnEnd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEnd.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnd.ForeColor = System.Drawing.Color.LavenderBlush;
            this.btnEnd.Location = new System.Drawing.Point(490, 42);
            this.btnEnd.Name = "btnEnd";
            this.btnEnd.Size = new System.Drawing.Size(82, 33);
            this.btnEnd.TabIndex = 12;
            this.btnEnd.Text = "Заврши";
            this.btnEnd.UseVisualStyleBackColor = false;
            this.btnEnd.Click += new System.EventHandler(this.btnEnd_Click);
            // 
            // lblStudentInfo
            // 
            this.lblStudentInfo.AutoSize = true;
            this.lblStudentInfo.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStudentInfo.Location = new System.Drawing.Point(15, 18);
            this.lblStudentInfo.Name = "lblStudentInfo";
            this.lblStudentInfo.Size = new System.Drawing.Size(0, 21);
            this.lblStudentInfo.TabIndex = 13;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblTestName);
            this.panel1.Controls.Add(this.lblTimer);
            this.panel1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(12, 42);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(472, 33);
            this.panel1.TabIndex = 14;
            // 
            // lstViewQuestions
            // 
            this.lstViewQuestions.BackColor = System.Drawing.Color.LavenderBlush;
            this.lstViewQuestions.HideSelection = false;
            this.lstViewQuestions.Location = new System.Drawing.Point(12, 107);
            this.lstViewQuestions.MultiSelect = false;
            this.lstViewQuestions.Name = "lstViewQuestions";
            this.lstViewQuestions.OwnerDraw = true;
            this.lstViewQuestions.Size = new System.Drawing.Size(73, 297);
            this.lstViewQuestions.TabIndex = 15;
            this.lstViewQuestions.UseCompatibleStateImageBehavior = false;
            this.lstViewQuestions.DrawItem += new System.Windows.Forms.DrawListViewItemEventHandler(this.lstViewQuestions_DrawItem);
            this.lstViewQuestions.SelectedIndexChanged += new System.EventHandler(this.lstViewQuestions_SelectedIndexChanged);
            // 
            // FormStudentTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.ClientSize = new System.Drawing.Size(584, 462);
            this.Controls.Add(this.lstViewQuestions);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblStudentInfo);
            this.Controls.Add(this.btnEnd);
            this.Controls.Add(this.btnPrevious);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.grpBoxQuestion);
            this.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.Name = "FormStudentTest";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "<OnlineKolokvium - Студент - TestName>  ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormStudentTest_FormClosing);
            this.Load += new System.EventHandler(this.FormStudentTest_Load);
            this.grpBoxQuestion.ResumeLayout(false);
            this.grpBoxQuestion.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblTimer;
        private System.Windows.Forms.Label lblTestName;
        private System.Windows.Forms.GroupBox grpBoxQuestion;
        private System.Windows.Forms.TextBox txtBoxQuestion;
        private System.Windows.Forms.FlowLayoutPanel flwLayoutAnswers;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnPrevious;
        private System.Windows.Forms.Button btnEnd;
        private System.Windows.Forms.Label lblStudentInfo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListView lstViewQuestions;
        private System.Windows.Forms.Timer timer1;
    }
}