﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OnlineKolokviumLibrary;
using OnlineKolokviumLibrary.Models;

namespace OnlineKolokviumUI
{
    public partial class FormProfesorCreateEditTest : Form
    {
        bool IsCreateMode;
        bool IsBtnCancelClicked = false;
        bool IsBtnCreateEditClicked = false;

        List<SubjectModel> subjects;
        SubjectModel subject;

        // this form has to create or update these
        TestModel test;
        List<QuestionModel> questions;
        // current number in tuple
        int curr = 0;



        public FormProfesorCreateEditTest(List<SubjectModel> subjects, SubjectModel subject)
        {
            InitializeComponent();
            this.Text = "OnlineKolokvium - Професор - Креирај тест";
            IsCreateMode = true;
            this.subjects = subjects;
            this.subject = subject;
            questions = new List<QuestionModel>();
            
        }

        public FormProfesorCreateEditTest(List<SubjectModel> subjects, TestModel test, SubjectModel subject)
        {
            InitializeComponent();
            this.Text = "OnlineKolokvium - Професор - Уреди тест";
            IsCreateMode = false;
            this.subjects = subjects;
            this.test = test;
            this.subject = subject;
            questions = ProgramHelper.Db.GetQuestionsByTest(test.ID);
            for(int i = 0; i < questions.Count; i++)
            {
                questions[i].Answers = ProgramHelper.Db.GetAnswersByQuestion(questions[i].ID);
            }
            
        }

        private void FormProfesorCreateEditTest_Load(object sender, EventArgs e)
        {
            FillCmbBoxSubject();
            FillLstBoxQuestions();

            if (IsCreateMode)
            {
                btnCreateEditTest.Text = "Креирај";

            }
            else
            {
                FillTestData();
            }

            txtBoxSession.Text = ProgramHelper.GenerateSessionString(dtpExamDate.Value);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Промените нема да се зачуваат. Дали сакате да продолжите?", "Потврда", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {
                IsBtnCancelClicked = true;
                this.Close();
            }
            else
            {
                IsBtnCancelClicked = false;
            }

        }

        private void FormProfesorCreateEditTest_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsBtnCreateEditClicked)
            {
                if (MessageBox.Show("Дали сакате овие промени да се зачуваат?", "Потврда", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.Cancel)
                {
                    IsBtnCreateEditClicked = false;
                    e.Cancel = true;
                }
                UpdateData();
                DialogResult = DialogResult.OK;
            }
            else if (!IsBtnCancelClicked)
            {
                if (MessageBox.Show("Промените нема да се зачуваат. Дали сакате да продолжите?", "Потврда", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
                DialogResult = DialogResult.Cancel;
            }
            else
            {
                DialogResult = DialogResult.Cancel;
            }


        }

        private void btnCreateEditTest_Click(object sender, EventArgs e)
        {
            if (ValidateData())
            {
                IsBtnCreateEditClicked = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("Имате внесено невалидни податоци за некои полиња.", "Грешка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void FillCmbBoxSubject()
        {
            cmbBoxSubject.DataSource = subjects;
            cmbBoxSubject.DisplayMember = "Name";
            if (subject != null)
                cmbBoxSubject.SelectedItem = subject;
        }

        private void FillLstBoxQuestions()
        {
            lstBoxQuestions.DataSource = null;
            lstBoxQuestions.DataSource = questions;
            
            lstBoxQuestions.DisplayMember = "Text";
            lstBoxQuestions.ClearSelected();
        }

        private void FillTestData()
        {
            btnCreateEditTest.Text = "Зачувај";
            txtBoxName.Text = test.Name;
            txtBoxSession.Text = test.Session;
            dtpExamDate.Value = test.Date;
            chckBoxActive.Checked = test.IsActive;
            numUpDownMinutes.Value = test.Duration;
        }

        private bool ValidateData()
        {
            if (txtBoxName.Text == "" || txtBoxName.Text == null)
                return false;
            return true;
        }

        private void UpdateData()
        {
            UpdateTest();
            UpdateQuestions();
            UpdateAnswers();
        }

        private void UpdateAnswers()
        {
            for(int i = 0; i < questions.Count; i++)
            {
                for(int j = 0; j < questions[i].Answers.Count; j++)
                {
                    if(questions[i].Answers[j].ID == -1)
                    {
                        questions[i].Answers[j].QuestionID = questions[i].ID;
                        ProgramHelper.Db.CreateAnswer(questions[i].Answers[j]);
                    }
                    else
                    {
                        ProgramHelper.Db.UpdateAnswer(questions[i].Answers[j]);
                    }
                }
            }
        }

        private void UpdateQuestions()
        {
            for(int i = 0; i < questions.Count; i++)
            {
                // a new question
                if(questions[i].ID == -1)
                {
                    questions[i].TestID = test.ID;
                    ProgramHelper.Db.CreateQuestion(questions[i]);
                }
                // update existing
                else
                {
                    ProgramHelper.Db.UpdateQuestion(questions[i]);
                }
            }
        }

        private void UpdateTest()
        {

            SubjectModel s = (SubjectModel)cmbBoxSubject.SelectedValue;
            string sessionString = txtBoxSession.Text;
            int sessionID = ProgramHelper.Db.GetAndSetSessionID(sessionString);
            DateTime date = dtpExamDate.Value;
            bool isActive = chckBoxActive.Checked;
            string name = txtBoxName.Text;
            if (IsCreateMode)
            {
                test = new TestModel(-1, name, s.ID, date, sessionID, isActive, (int)numUpDownMinutes.Value);
                test.Session = sessionString;
                //test.Questions = questions;
                // should update ID
                ProgramHelper.Db.CreateTest(test);
            }
            else
            {
                test.SubjectID = s.ID;
                test.TestSessionID = sessionID;
                test.Session = sessionString;
                test.Date = date;
                test.Name = name;
                test.IsActive = isActive;
                test.Duration = (int)numUpDownMinutes.Value;
                ProgramHelper.Db.UpdateTest(test);
            }
        }

        private void dtpExamDate_ValueChanged(object sender, EventArgs e)
        {
            txtBoxSession.Text = ProgramHelper.GenerateSessionString(dtpExamDate.Value);
        }

        private void btnNewQuestion_Click(object sender, EventArgs e)
        {
            FormProfesorCreateEditQuestion frmProfesorCreateEditQuestion = new FormProfesorCreateEditQuestion();
            if (frmProfesorCreateEditQuestion.ShowDialog() == DialogResult.OK)
            {
                
                QuestionModel tmp = frmProfesorCreateEditQuestion.question;
                curr++;
                questions.Add(tmp);
                FillLstBoxQuestions();
            }
        }

        private void btnEditQuestion_Click(object sender, EventArgs e)
        {
            QuestionModel question = ((QuestionModel)lstBoxQuestions.SelectedItem);
            FormProfesorCreateEditQuestion frmProfesorCreateEditQuestion = new FormProfesorCreateEditQuestion(question);

            if (frmProfesorCreateEditQuestion.ShowDialog() == DialogResult.OK)
            {
                FillLstBoxQuestions();
            }
        }

        private void lstBoxQuestions_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstBoxQuestions.SelectedIndex >= 0)
            {
                btnDeleteQuestion.Enabled = true;
                btnEditQuestion.Enabled = true;
            }
            else
            {
                btnDeleteQuestion.Enabled = false;
                btnEditQuestion.Enabled = false;
            }
        }

        private void btnDeleteQuestion_Click(object sender, EventArgs e)
        {
            
            if (MessageBox.Show("Дали сте сигурни дека сакате да го избришите прашањето?", "Потврда", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {
                // if in create mode, delete from questions and addedQuestions
                
                QuestionModel doomed = ((QuestionModel)lstBoxQuestions.SelectedItem);
                if(doomed.ID != -1)
                {
                    ProgramHelper.Db.DeleteQuestion(doomed);
                }
                questions.Remove(doomed);

                FillLstBoxQuestions();
            }
            

        }
    }
}
