﻿namespace OnlineKolokviumUI
{
    partial class FormProfesorCreateEditTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMessageName = new System.Windows.Forms.Label();
            this.txtBoxName = new System.Windows.Forms.TextBox();
            this.lblMessageExamDate = new System.Windows.Forms.Label();
            this.dtpExamDate = new System.Windows.Forms.DateTimePicker();
            this.lstBoxQuestions = new System.Windows.Forms.ListBox();
            this.btnNewQuestion = new System.Windows.Forms.Button();
            this.btnEditQuestion = new System.Windows.Forms.Button();
            this.btnDeleteQuestion = new System.Windows.Forms.Button();
            this.btnCreateEditTest = new System.Windows.Forms.Button();
            this.lblMessageQuestions = new System.Windows.Forms.Label();
            this.txtBoxSession = new System.Windows.Forms.TextBox();
            this.lblMessageSession = new System.Windows.Forms.Label();
            this.chckBoxActive = new System.Windows.Forms.CheckBox();
            this.cmbBoxSubject = new System.Windows.Forms.ComboBox();
            this.lblMessageSubject = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.numUpDownMinutes = new System.Windows.Forms.NumericUpDown();
            this.lblMessageMinutes = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numUpDownMinutes)).BeginInit();
            this.SuspendLayout();
            // 
            // lblMessageName
            // 
            this.lblMessageName.AutoSize = true;
            this.lblMessageName.Location = new System.Drawing.Point(63, 24);
            this.lblMessageName.Name = "lblMessageName";
            this.lblMessageName.Size = new System.Drawing.Size(55, 30);
            this.lblMessageName.TabIndex = 0;
            this.lblMessageName.Text = "Име";
            // 
            // txtBoxName
            // 
            this.txtBoxName.Location = new System.Drawing.Point(67, 57);
            this.txtBoxName.Name = "txtBoxName";
            this.txtBoxName.Size = new System.Drawing.Size(239, 35);
            this.txtBoxName.TabIndex = 1;
            // 
            // lblMessageExamDate
            // 
            this.lblMessageExamDate.AutoSize = true;
            this.lblMessageExamDate.Location = new System.Drawing.Point(62, 95);
            this.lblMessageExamDate.Name = "lblMessageExamDate";
            this.lblMessageExamDate.Size = new System.Drawing.Size(73, 30);
            this.lblMessageExamDate.TabIndex = 2;
            this.lblMessageExamDate.Text = "Датум";
            // 
            // dtpExamDate
            // 
            this.dtpExamDate.CalendarFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpExamDate.CustomFormat = "dd-MM-yyyy";
            this.dtpExamDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExamDate.Location = new System.Drawing.Point(68, 128);
            this.dtpExamDate.Name = "dtpExamDate";
            this.dtpExamDate.Size = new System.Drawing.Size(239, 35);
            this.dtpExamDate.TabIndex = 4;
            this.dtpExamDate.ValueChanged += new System.EventHandler(this.dtpExamDate_ValueChanged);
            // 
            // lstBoxQuestions
            // 
            this.lstBoxQuestions.BackColor = System.Drawing.Color.LavenderBlush;
            this.lstBoxQuestions.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstBoxQuestions.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lstBoxQuestions.FormattingEnabled = true;
            this.lstBoxQuestions.ItemHeight = 21;
            this.lstBoxQuestions.Location = new System.Drawing.Point(67, 199);
            this.lstBoxQuestions.Name = "lstBoxQuestions";
            this.lstBoxQuestions.Size = new System.Drawing.Size(239, 193);
            this.lstBoxQuestions.TabIndex = 5;
            this.lstBoxQuestions.SelectedIndexChanged += new System.EventHandler(this.lstBoxQuestions_SelectedIndexChanged);
            // 
            // btnNewQuestion
            // 
            this.btnNewQuestion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewQuestion.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewQuestion.ForeColor = System.Drawing.SystemColors.GrayText;
            this.btnNewQuestion.Location = new System.Drawing.Point(312, 199);
            this.btnNewQuestion.Name = "btnNewQuestion";
            this.btnNewQuestion.Size = new System.Drawing.Size(170, 40);
            this.btnNewQuestion.TabIndex = 21;
            this.btnNewQuestion.Text = "Ново прашање";
            this.btnNewQuestion.UseVisualStyleBackColor = true;
            this.btnNewQuestion.Click += new System.EventHandler(this.btnNewQuestion_Click);
            // 
            // btnEditQuestion
            // 
            this.btnEditQuestion.Enabled = false;
            this.btnEditQuestion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditQuestion.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditQuestion.ForeColor = System.Drawing.SystemColors.GrayText;
            this.btnEditQuestion.Location = new System.Drawing.Point(312, 245);
            this.btnEditQuestion.Name = "btnEditQuestion";
            this.btnEditQuestion.Size = new System.Drawing.Size(170, 40);
            this.btnEditQuestion.TabIndex = 22;
            this.btnEditQuestion.Text = "Уреди прашање";
            this.btnEditQuestion.UseVisualStyleBackColor = true;
            this.btnEditQuestion.Click += new System.EventHandler(this.btnEditQuestion_Click);
            // 
            // btnDeleteQuestion
            // 
            this.btnDeleteQuestion.Enabled = false;
            this.btnDeleteQuestion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteQuestion.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteQuestion.ForeColor = System.Drawing.SystemColors.GrayText;
            this.btnDeleteQuestion.Location = new System.Drawing.Point(312, 291);
            this.btnDeleteQuestion.Name = "btnDeleteQuestion";
            this.btnDeleteQuestion.Size = new System.Drawing.Size(170, 40);
            this.btnDeleteQuestion.TabIndex = 23;
            this.btnDeleteQuestion.Text = "Избриши прашање";
            this.btnDeleteQuestion.UseVisualStyleBackColor = true;
            this.btnDeleteQuestion.Click += new System.EventHandler(this.btnDeleteQuestion_Click);
            // 
            // btnCreateEditTest
            // 
            this.btnCreateEditTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCreateEditTest.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateEditTest.ForeColor = System.Drawing.SystemColors.GrayText;
            this.btnCreateEditTest.Location = new System.Drawing.Point(67, 398);
            this.btnCreateEditTest.Name = "btnCreateEditTest";
            this.btnCreateEditTest.Size = new System.Drawing.Size(119, 40);
            this.btnCreateEditTest.TabIndex = 24;
            this.btnCreateEditTest.Text = "<Креирај/Зачувај тест>";
            this.btnCreateEditTest.UseVisualStyleBackColor = true;
            this.btnCreateEditTest.Click += new System.EventHandler(this.btnCreateEditTest_Click);
            // 
            // lblMessageQuestions
            // 
            this.lblMessageQuestions.AutoSize = true;
            this.lblMessageQuestions.Location = new System.Drawing.Point(63, 166);
            this.lblMessageQuestions.Name = "lblMessageQuestions";
            this.lblMessageQuestions.Size = new System.Drawing.Size(107, 30);
            this.lblMessageQuestions.TabIndex = 25;
            this.lblMessageQuestions.Text = "Прашања";
            // 
            // txtBoxSession
            // 
            this.txtBoxSession.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoxSession.Location = new System.Drawing.Point(311, 128);
            this.txtBoxSession.Name = "txtBoxSession";
            this.txtBoxSession.Size = new System.Drawing.Size(169, 29);
            this.txtBoxSession.TabIndex = 26;
            // 
            // lblMessageSession
            // 
            this.lblMessageSession.AutoSize = true;
            this.lblMessageSession.Location = new System.Drawing.Point(306, 95);
            this.lblMessageSession.Name = "lblMessageSession";
            this.lblMessageSession.Size = new System.Drawing.Size(75, 30);
            this.lblMessageSession.TabIndex = 27;
            this.lblMessageSession.Text = "Сесија";
            // 
            // chckBoxActive
            // 
            this.chckBoxActive.AutoSize = true;
            this.chckBoxActive.Location = new System.Drawing.Point(371, 337);
            this.chckBoxActive.Name = "chckBoxActive";
            this.chckBoxActive.Size = new System.Drawing.Size(111, 34);
            this.chckBoxActive.TabIndex = 28;
            this.chckBoxActive.Text = "Активен";
            this.chckBoxActive.UseVisualStyleBackColor = true;
            // 
            // cmbBoxSubject
            // 
            this.cmbBoxSubject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoxSubject.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBoxSubject.FormattingEnabled = true;
            this.cmbBoxSubject.Location = new System.Drawing.Point(311, 57);
            this.cmbBoxSubject.Name = "cmbBoxSubject";
            this.cmbBoxSubject.Size = new System.Drawing.Size(170, 29);
            this.cmbBoxSubject.TabIndex = 29;
            // 
            // lblMessageSubject
            // 
            this.lblMessageSubject.AutoSize = true;
            this.lblMessageSubject.Location = new System.Drawing.Point(306, 24);
            this.lblMessageSubject.Name = "lblMessageSubject";
            this.lblMessageSubject.Size = new System.Drawing.Size(97, 30);
            this.lblMessageSubject.TabIndex = 30;
            this.lblMessageSubject.Text = "Предмет";
            // 
            // btnCancel
            // 
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.SystemColors.GrayText;
            this.btnCancel.Location = new System.Drawing.Point(187, 398);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(119, 40);
            this.btnCancel.TabIndex = 31;
            this.btnCancel.Text = "Отфрли";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // numUpDownMinutes
            // 
            this.numUpDownMinutes.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numUpDownMinutes.Location = new System.Drawing.Point(312, 363);
            this.numUpDownMinutes.Name = "numUpDownMinutes";
            this.numUpDownMinutes.Size = new System.Drawing.Size(70, 29);
            this.numUpDownMinutes.TabIndex = 32;
            // 
            // lblMessageMinutes
            // 
            this.lblMessageMinutes.AutoSize = true;
            this.lblMessageMinutes.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessageMinutes.Location = new System.Drawing.Point(388, 371);
            this.lblMessageMinutes.Name = "lblMessageMinutes";
            this.lblMessageMinutes.Size = new System.Drawing.Size(63, 21);
            this.lblMessageMinutes.TabIndex = 33;
            this.lblMessageMinutes.Text = "минути";
            // 
            // FormProfesorCreateEditTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.ClientSize = new System.Drawing.Size(544, 462);
            this.Controls.Add(this.lblMessageMinutes);
            this.Controls.Add(this.numUpDownMinutes);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblMessageSubject);
            this.Controls.Add(this.cmbBoxSubject);
            this.Controls.Add(this.chckBoxActive);
            this.Controls.Add(this.lblMessageSession);
            this.Controls.Add(this.txtBoxSession);
            this.Controls.Add(this.lblMessageQuestions);
            this.Controls.Add(this.btnCreateEditTest);
            this.Controls.Add(this.btnDeleteQuestion);
            this.Controls.Add(this.btnEditQuestion);
            this.Controls.Add(this.btnNewQuestion);
            this.Controls.Add(this.lstBoxQuestions);
            this.Controls.Add(this.dtpExamDate);
            this.Controls.Add(this.lblMessageExamDate);
            this.Controls.Add(this.txtBoxName);
            this.Controls.Add(this.lblMessageName);
            this.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.GrayText;
            this.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.Name = "FormProfesorCreateEditTest";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "<OnlineKolokvium - Професор - Креирај/Уреди тест>";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormProfesorCreateEditTest_FormClosing);
            this.Load += new System.EventHandler(this.FormProfesorCreateEditTest_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numUpDownMinutes)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMessageName;
        private System.Windows.Forms.TextBox txtBoxName;
        private System.Windows.Forms.Label lblMessageExamDate;
        private System.Windows.Forms.DateTimePicker dtpExamDate;
        private System.Windows.Forms.ListBox lstBoxQuestions;
        private System.Windows.Forms.Button btnNewQuestion;
        private System.Windows.Forms.Button btnEditQuestion;
        private System.Windows.Forms.Button btnDeleteQuestion;
        private System.Windows.Forms.Button btnCreateEditTest;
        private System.Windows.Forms.Label lblMessageQuestions;
        private System.Windows.Forms.TextBox txtBoxSession;
        private System.Windows.Forms.Label lblMessageSession;
        private System.Windows.Forms.CheckBox chckBoxActive;
        private System.Windows.Forms.ComboBox cmbBoxSubject;
        private System.Windows.Forms.Label lblMessageSubject;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.NumericUpDown numUpDownMinutes;
        private System.Windows.Forms.Label lblMessageMinutes;
    }
}