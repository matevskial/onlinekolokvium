﻿namespace OnlineKolokviumUI
{
    partial class FormProfesorTestStatistics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTestName = new System.Windows.Forms.Label();
            this.lblMessageStudentsList = new System.Windows.Forms.Label();
            this.chckBoxStudentsPassed = new System.Windows.Forms.CheckBox();
            this.lstBoxStudents = new System.Windows.Forms.ListBox();
            this.grpBoxStudentInfo = new System.Windows.Forms.GroupBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblUnansweredQuestions = new System.Windows.Forms.Label();
            this.lblMessageStatus = new System.Windows.Forms.Label();
            this.lblMessageUnansweredQuestions = new System.Windows.Forms.Label();
            this.lblWrongQuestions = new System.Windows.Forms.Label();
            this.lblMessageWrongQuestions = new System.Windows.Forms.Label();
            this.lblCorrectQuestions = new System.Windows.Forms.Label();
            this.lblMessageCorrectQuestions = new System.Windows.Forms.Label();
            this.grpBoxStatistics = new System.Windows.Forms.GroupBox();
            this.lblStudentsFailed = new System.Windows.Forms.Label();
            this.lblStudentsPassed = new System.Windows.Forms.Label();
            this.lblStudentsNumber = new System.Windows.Forms.Label();
            this.lblMessageStudentsFailed = new System.Windows.Forms.Label();
            this.lblMessageStudentsPassed = new System.Windows.Forms.Label();
            this.lblMessageStudentsNumber = new System.Windows.Forms.Label();
            this.grpBoxStudentInfo.SuspendLayout();
            this.grpBoxStatistics.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblTestName
            // 
            this.lblTestName.AutoSize = true;
            this.lblTestName.Location = new System.Drawing.Point(12, 11);
            this.lblTestName.Name = "lblTestName";
            this.lblTestName.Size = new System.Drawing.Size(135, 30);
            this.lblTestName.TabIndex = 0;
            this.lblTestName.Text = "<TestName>";
            // 
            // lblMessageStudentsList
            // 
            this.lblMessageStudentsList.AutoSize = true;
            this.lblMessageStudentsList.Location = new System.Drawing.Point(12, 47);
            this.lblMessageStudentsList.Name = "lblMessageStudentsList";
            this.lblMessageStudentsList.Size = new System.Drawing.Size(233, 30);
            this.lblMessageStudentsList.TabIndex = 1;
            this.lblMessageStudentsList.Text = "Студенти кои полагале";
            // 
            // chckBoxStudentsPassed
            // 
            this.chckBoxStudentsPassed.AutoSize = true;
            this.chckBoxStudentsPassed.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckBoxStudentsPassed.Location = new System.Drawing.Point(17, 80);
            this.chckBoxStudentsPassed.Name = "chckBoxStudentsPassed";
            this.chckBoxStudentsPassed.Size = new System.Drawing.Size(209, 25);
            this.chckBoxStudentsPassed.TabIndex = 2;
            this.chckBoxStudentsPassed.Text = "само положени студенти";
            this.chckBoxStudentsPassed.UseVisualStyleBackColor = true;
            this.chckBoxStudentsPassed.CheckedChanged += new System.EventHandler(this.chckBoxStudentsPassed_CheckedChanged);
            // 
            // lstBoxStudents
            // 
            this.lstBoxStudents.BackColor = System.Drawing.Color.LavenderBlush;
            this.lstBoxStudents.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstBoxStudents.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lstBoxStudents.FormattingEnabled = true;
            this.lstBoxStudents.ItemHeight = 21;
            this.lstBoxStudents.Location = new System.Drawing.Point(17, 111);
            this.lstBoxStudents.Name = "lstBoxStudents";
            this.lstBoxStudents.Size = new System.Drawing.Size(228, 193);
            this.lstBoxStudents.TabIndex = 13;
            this.lstBoxStudents.SelectedIndexChanged += new System.EventHandler(this.lstBoxStudents_SelectedIndexChanged);
            this.lstBoxStudents.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.lstBoxStudents_Format);
            // 
            // grpBoxStudentInfo
            // 
            this.grpBoxStudentInfo.Controls.Add(this.lblStatus);
            this.grpBoxStudentInfo.Controls.Add(this.lblUnansweredQuestions);
            this.grpBoxStudentInfo.Controls.Add(this.lblMessageStatus);
            this.grpBoxStudentInfo.Controls.Add(this.lblMessageUnansweredQuestions);
            this.grpBoxStudentInfo.Controls.Add(this.lblWrongQuestions);
            this.grpBoxStudentInfo.Controls.Add(this.lblMessageWrongQuestions);
            this.grpBoxStudentInfo.Controls.Add(this.lblCorrectQuestions);
            this.grpBoxStudentInfo.Controls.Add(this.lblMessageCorrectQuestions);
            this.grpBoxStudentInfo.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBoxStudentInfo.Location = new System.Drawing.Point(304, 98);
            this.grpBoxStudentInfo.Name = "grpBoxStudentInfo";
            this.grpBoxStudentInfo.Size = new System.Drawing.Size(228, 206);
            this.grpBoxStudentInfo.TabIndex = 14;
            this.grpBoxStudentInfo.TabStop = false;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(72, 157);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 21);
            this.lblStatus.TabIndex = 14;
            // 
            // lblUnansweredQuestions
            // 
            this.lblUnansweredQuestions.AutoSize = true;
            this.lblUnansweredQuestions.Location = new System.Drawing.Point(129, 121);
            this.lblUnansweredQuestions.Name = "lblUnansweredQuestions";
            this.lblUnansweredQuestions.Size = new System.Drawing.Size(0, 21);
            this.lblUnansweredQuestions.TabIndex = 13;
            // 
            // lblMessageStatus
            // 
            this.lblMessageStatus.AutoSize = true;
            this.lblMessageStatus.Location = new System.Drawing.Point(6, 157);
            this.lblMessageStatus.Name = "lblMessageStatus";
            this.lblMessageStatus.Size = new System.Drawing.Size(60, 21);
            this.lblMessageStatus.TabIndex = 12;
            this.lblMessageStatus.Text = "Статус:";
            // 
            // lblMessageUnansweredQuestions
            // 
            this.lblMessageUnansweredQuestions.AutoSize = true;
            this.lblMessageUnansweredQuestions.Location = new System.Drawing.Point(6, 121);
            this.lblMessageUnansweredQuestions.Name = "lblMessageUnansweredQuestions";
            this.lblMessageUnansweredQuestions.Size = new System.Drawing.Size(117, 21);
            this.lblMessageUnansweredQuestions.TabIndex = 11;
            this.lblMessageUnansweredQuestions.Text = "Неодговорени:";
            // 
            // lblWrongQuestions
            // 
            this.lblWrongQuestions.AutoSize = true;
            this.lblWrongQuestions.Location = new System.Drawing.Point(176, 86);
            this.lblWrongQuestions.Name = "lblWrongQuestions";
            this.lblWrongQuestions.Size = new System.Drawing.Size(0, 21);
            this.lblWrongQuestions.TabIndex = 10;
            // 
            // lblMessageWrongQuestions
            // 
            this.lblMessageWrongQuestions.AutoSize = true;
            this.lblMessageWrongQuestions.Location = new System.Drawing.Point(6, 86);
            this.lblMessageWrongQuestions.Name = "lblMessageWrongQuestions";
            this.lblMessageWrongQuestions.Size = new System.Drawing.Size(164, 21);
            this.lblMessageWrongQuestions.TabIndex = 9;
            this.lblMessageWrongQuestions.Text = "Неточно одговорени:";
            // 
            // lblCorrectQuestions
            // 
            this.lblCorrectQuestions.AutoSize = true;
            this.lblCorrectQuestions.Location = new System.Drawing.Point(158, 46);
            this.lblCorrectQuestions.Name = "lblCorrectQuestions";
            this.lblCorrectQuestions.Size = new System.Drawing.Size(0, 21);
            this.lblCorrectQuestions.TabIndex = 8;
            // 
            // lblMessageCorrectQuestions
            // 
            this.lblMessageCorrectQuestions.AutoSize = true;
            this.lblMessageCorrectQuestions.Location = new System.Drawing.Point(6, 46);
            this.lblMessageCorrectQuestions.Name = "lblMessageCorrectQuestions";
            this.lblMessageCorrectQuestions.Size = new System.Drawing.Size(146, 21);
            this.lblMessageCorrectQuestions.TabIndex = 7;
            this.lblMessageCorrectQuestions.Text = "Точно одговорени:";
            // 
            // grpBoxStatistics
            // 
            this.grpBoxStatistics.Controls.Add(this.lblStudentsFailed);
            this.grpBoxStatistics.Controls.Add(this.lblStudentsPassed);
            this.grpBoxStatistics.Controls.Add(this.lblStudentsNumber);
            this.grpBoxStatistics.Controls.Add(this.lblMessageStudentsFailed);
            this.grpBoxStatistics.Controls.Add(this.lblMessageStudentsPassed);
            this.grpBoxStatistics.Controls.Add(this.lblMessageStudentsNumber);
            this.grpBoxStatistics.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBoxStatistics.Location = new System.Drawing.Point(17, 310);
            this.grpBoxStatistics.Name = "grpBoxStatistics";
            this.grpBoxStatistics.Size = new System.Drawing.Size(515, 102);
            this.grpBoxStatistics.TabIndex = 15;
            this.grpBoxStatistics.TabStop = false;
            this.grpBoxStatistics.Text = "Статистика";
            // 
            // lblStudentsFailed
            // 
            this.lblStudentsFailed.AutoSize = true;
            this.lblStudentsFailed.Location = new System.Drawing.Point(395, 29);
            this.lblStudentsFailed.Name = "lblStudentsFailed";
            this.lblStudentsFailed.Size = new System.Drawing.Size(19, 21);
            this.lblStudentsFailed.TabIndex = 5;
            this.lblStudentsFailed.Text = "0";
            // 
            // lblStudentsPassed
            // 
            this.lblStudentsPassed.AutoSize = true;
            this.lblStudentsPassed.Location = new System.Drawing.Point(153, 61);
            this.lblStudentsPassed.Name = "lblStudentsPassed";
            this.lblStudentsPassed.Size = new System.Drawing.Size(19, 21);
            this.lblStudentsPassed.TabIndex = 4;
            this.lblStudentsPassed.Text = "0";
            // 
            // lblStudentsNumber
            // 
            this.lblStudentsNumber.AutoSize = true;
            this.lblStudentsNumber.Location = new System.Drawing.Point(153, 29);
            this.lblStudentsNumber.Name = "lblStudentsNumber";
            this.lblStudentsNumber.Size = new System.Drawing.Size(19, 21);
            this.lblStudentsNumber.TabIndex = 3;
            this.lblStudentsNumber.Text = "0";
            // 
            // lblMessageStudentsFailed
            // 
            this.lblMessageStudentsFailed.AutoSize = true;
            this.lblMessageStudentsFailed.Location = new System.Drawing.Point(231, 29);
            this.lblMessageStudentsFailed.Name = "lblMessageStudentsFailed";
            this.lblMessageStudentsFailed.Size = new System.Drawing.Size(158, 21);
            this.lblMessageStudentsFailed.TabIndex = 2;
            this.lblMessageStudentsFailed.Text = "Вкупно неположиле:";
            // 
            // lblMessageStudentsPassed
            // 
            this.lblMessageStudentsPassed.AutoSize = true;
            this.lblMessageStudentsPassed.Location = new System.Drawing.Point(6, 61);
            this.lblMessageStudentsPassed.Name = "lblMessageStudentsPassed";
            this.lblMessageStudentsPassed.Size = new System.Drawing.Size(141, 21);
            this.lblMessageStudentsPassed.TabIndex = 1;
            this.lblMessageStudentsPassed.Text = "Вкупно положиле:";
            // 
            // lblMessageStudentsNumber
            // 
            this.lblMessageStudentsNumber.AutoSize = true;
            this.lblMessageStudentsNumber.Location = new System.Drawing.Point(7, 29);
            this.lblMessageStudentsNumber.Name = "lblMessageStudentsNumber";
            this.lblMessageStudentsNumber.Size = new System.Drawing.Size(133, 21);
            this.lblMessageStudentsNumber.TabIndex = 0;
            this.lblMessageStudentsNumber.Text = "Вкупно полагале:";
            // 
            // FormProfesorTestStatistics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.ClientSize = new System.Drawing.Size(544, 422);
            this.Controls.Add(this.grpBoxStatistics);
            this.Controls.Add(this.grpBoxStudentInfo);
            this.Controls.Add(this.lstBoxStudents);
            this.Controls.Add(this.chckBoxStudentsPassed);
            this.Controls.Add(this.lblMessageStudentsList);
            this.Controls.Add(this.lblTestName);
            this.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.GrayText;
            this.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.Name = "FormProfesorTestStatistics";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "<OnlineKolokvium - Професор - TestName>";
            this.Load += new System.EventHandler(this.FormProfesorTestStatistics_Load);
            this.grpBoxStudentInfo.ResumeLayout(false);
            this.grpBoxStudentInfo.PerformLayout();
            this.grpBoxStatistics.ResumeLayout(false);
            this.grpBoxStatistics.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTestName;
        private System.Windows.Forms.Label lblMessageStudentsList;
        private System.Windows.Forms.CheckBox chckBoxStudentsPassed;
        private System.Windows.Forms.ListBox lstBoxStudents;
        private System.Windows.Forms.GroupBox grpBoxStudentInfo;
        private System.Windows.Forms.GroupBox grpBoxStatistics;
        private System.Windows.Forms.Label lblStudentsNumber;
        private System.Windows.Forms.Label lblMessageStudentsFailed;
        private System.Windows.Forms.Label lblMessageStudentsPassed;
        private System.Windows.Forms.Label lblMessageStudentsNumber;
        private System.Windows.Forms.Label lblMessageCorrectQuestions;
        private System.Windows.Forms.Label lblStudentsFailed;
        private System.Windows.Forms.Label lblStudentsPassed;
        private System.Windows.Forms.Label lblMessageStatus;
        private System.Windows.Forms.Label lblMessageUnansweredQuestions;
        private System.Windows.Forms.Label lblWrongQuestions;
        private System.Windows.Forms.Label lblMessageWrongQuestions;
        private System.Windows.Forms.Label lblCorrectQuestions;
        private System.Windows.Forms.Label lblUnansweredQuestions;
        private System.Windows.Forms.Label lblStatus;
    }
}