﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OnlineKolokviumLibrary;
using OnlineKolokviumLibrary.Models;
using System.Windows.Forms;

namespace OnlineKolokviumUI
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void btnStudent_Click(object sender, EventArgs e)
        {
            FormStudentLogin frmStudentLogin = new FormStudentLogin(this);
            frmStudentLogin.Show();
            this.Hide();
        }

        private void btnProfesor_Click(object sender, EventArgs e)
        {
            FormProfesorLogin frmProfesorLogin = new FormProfesorLogin(this);
            frmProfesorLogin.Show();
            this.Hide();
        }
    }
}
