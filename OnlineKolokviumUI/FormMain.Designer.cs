﻿namespace OnlineKolokviumUI
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnProfesor = new System.Windows.Forms.Button();
            this.btnStudent = new System.Windows.Forms.Button();
            this.lblMessageChooseRole = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnProfesor
            // 
            this.btnProfesor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProfesor.ForeColor = System.Drawing.SystemColors.GrayText;
            this.btnProfesor.Location = new System.Drawing.Point(161, 148);
            this.btnProfesor.Name = "btnProfesor";
            this.btnProfesor.Size = new System.Drawing.Size(150, 40);
            this.btnProfesor.TabIndex = 0;
            this.btnProfesor.Text = "Професор";
            this.btnProfesor.UseVisualStyleBackColor = true;
            this.btnProfesor.Click += new System.EventHandler(this.btnProfesor_Click);
            // 
            // btnStudent
            // 
            this.btnStudent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStudent.ForeColor = System.Drawing.SystemColors.GrayText;
            this.btnStudent.Location = new System.Drawing.Point(161, 254);
            this.btnStudent.Name = "btnStudent";
            this.btnStudent.Size = new System.Drawing.Size(150, 40);
            this.btnStudent.TabIndex = 1;
            this.btnStudent.Text = "Студент";
            this.btnStudent.UseVisualStyleBackColor = true;
            this.btnStudent.Click += new System.EventHandler(this.btnStudent_Click);
            // 
            // lblMessageChooseRole
            // 
            this.lblMessageChooseRole.AutoSize = true;
            this.lblMessageChooseRole.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lblMessageChooseRole.Location = new System.Drawing.Point(106, 69);
            this.lblMessageChooseRole.Name = "lblMessageChooseRole";
            this.lblMessageChooseRole.Size = new System.Drawing.Size(273, 30);
            this.lblMessageChooseRole.TabIndex = 2;
            this.lblMessageChooseRole.Text = "Изберете за да се најавите";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.ClientSize = new System.Drawing.Size(484, 362);
            this.Controls.Add(this.lblMessageChooseRole);
            this.Controls.Add(this.btnStudent);
            this.Controls.Add(this.btnProfesor);
            this.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OnlineKolokvium - Почетна";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnProfesor;
        private System.Windows.Forms.Button btnStudent;
        private System.Windows.Forms.Label lblMessageChooseRole;
    }
}

