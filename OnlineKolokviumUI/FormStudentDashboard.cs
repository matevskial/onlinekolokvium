﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OnlineKolokviumLibrary;
using OnlineKolokviumLibrary.Models;

namespace OnlineKolokviumUI
{
    public partial class FormStudentDashboard : Form
    {
        FormMain frmMain;
        StudentModel s;

        List<TestResultModel> results;
        List<TestModel> tests;
        List<TestSessionModel> sessions;

        List<TestModel> takenTests;
        List<TestModel> untakenTests;
        
        public FormStudentDashboard(FormMain frmMain, StudentModel s)
        {
            InitializeComponent();
            this.frmMain = frmMain;
            this.s = s;
        }

        private void FormStudentDashboard_Load(object sender, EventArgs e)
        {
            GetData();
            FilterData();
            FillData();
        }

        private void mnuStripLogout_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Дали сте сигурни дека сакате да се одјавите?", "Потврда", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                frmMain.Show();
                this.Close();
            } 
        }

        private void lstBoxTakenTests_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstBoxTakenTests.SelectedIndex >= 0)
                btnResults.Enabled = true;
            else
                btnResults.Enabled = false;
        }

        private void lstBoxUntakenTests_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstBoxUntakenTests.SelectedIndex >= 0)
            {
                TestModel t = ((TestModel)lstBoxUntakenTests.SelectedItem);
                btnTakeExam.Enabled = t.IsActive;
            }
            else
            {
                btnTakeExam.Enabled = false;
            }
        }

        private void cmbBoxSession_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbBoxSession.SelectedIndex >= 0)
            {
                FilterTakenTests();
                FillLstBoxTakenTests();
            }
        }

        private void chckBoxPassedSubjects_CheckedChanged(object sender, EventArgs e)
        {
            FilterTakenTests();
            FillLstBoxTakenTests();
        }

        private void btnResults_Click(object sender, EventArgs e)
        {
            TestModel t = ((TestModel)lstBoxTakenTests.SelectedItem);
            TestResultModel r = null;
            for(int i = 0; i < results.Count; i++)
            {
                if(results[i].TestID == t.ID)
                {
                    r = results[i];
                }
            }
            FormStudentTestResults frmStudentTestResults = new FormStudentTestResults(r);
            frmStudentTestResults.Show();
        }

        private void btnTakeExam_Click(object sender, EventArgs e)
        {
            TestModel test = ((TestModel)lstBoxUntakenTests.SelectedItem);
            FormStudentTest frmStudentTest = new FormStudentTest(this, s, test);
            this.Hide();
            if (frmStudentTest.ShowDialog() == DialogResult.OK)
            {
                this.Show();
                GetData();
                FilterData();
                FillData();
            }
        }

        private void GetData()
        {
            GetSessions();
            GetResults();
            GetTests();
        }

        private void GetSessions()
        {
            sessions = ProgramHelper.Db.GetAllSessions();
        }

        private void GetResults()
        {
            results = ProgramHelper.Db.GetTestResultsByStudent(s.ID);
        }

        private void GetTests()
        {
            tests = ProgramHelper.Db.GetAllTests();
        }

        private void FilterData()
        {
            FilterTakenTests();
            FilterUntakenTests();
        }

        private void FilterTakenTests()
        {
            takenTests = new List<TestModel>();
            
            TestSessionModel session = ((TestSessionModel)cmbBoxSession.SelectedItem);
            bool shouldFilterByPassed = chckBoxPassedSubjects.Checked;

            for(int i = 0; i < results.Count(); i++)
            {
                if(session != null)
                {
                    if(results[i].test.Session.Equals(session.Name))
                    {
                        if (shouldFilterByPassed)
                        {
                            if (results[i].IsPassed)
                                takenTests.Add(results[i].test);
                        }
                        else
                        {
                            takenTests.Add(results[i].test);
                        }
                       
                    }
                }
                else
                {
                    if (shouldFilterByPassed)
                    {
                        if(results[i].IsPassed)
                            takenTests.Add(results[i].test);
                    }
                    else
                    {
                        takenTests.Add(results[i].test);
                    }
                }
            }
        }

        private void FilterUntakenTests()
        {
            untakenTests = new List<TestModel>();
            for(int i = 0; i < tests.Count; i++)
            {
                bool IsUntaken = true;
                for(int j = 0; j < results.Count; j++)
                {
                    if(results[j].test.ID == tests[i].ID)
                    {
                        IsUntaken = false;
                        break;
                    }
                }
                if(IsUntaken)
                {
                    untakenTests.Add(tests[i]);
                }
            }
        }

        private void FillData()
        {
            FillStudentInfo();
            FillCmbBoxSession();
            FillLstBoxTakenTests();
            FillLstBoxUntakenTests();
        }

        private void FillStudentInfo()
        {
            lblStudentInfo.Text = s.FirstName + " " + s.LastName + " " + s.Index;
        }

        private void FillCmbBoxSession()
        {
            cmbBoxSession.DataSource = null;
            cmbBoxSession.DataSource = sessions;
            cmbBoxSession.DisplayMember = "Name";
        }

        private void FillLstBoxTakenTests()
        {
            lstBoxTakenTests.DataSource = null;
            lstBoxTakenTests.DataSource = takenTests;
            lstBoxTakenTests.DisplayMember = "Name";
            lstBoxTakenTests.ClearSelected();
        }

        private void FillLstBoxUntakenTests()
        {
            lstBoxUntakenTests.DataSource = null;
            lstBoxUntakenTests.DataSource = untakenTests;
            lstBoxUntakenTests.DisplayMember = "Name";
            lstBoxUntakenTests.ClearSelected();
        }
    }
}
