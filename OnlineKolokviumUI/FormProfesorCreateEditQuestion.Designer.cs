﻿namespace OnlineKolokviumUI
{
    partial class FormProfesorCreateEditQuestion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtBoxQuestion = new System.Windows.Forms.TextBox();
            this.lstBoxAnswers = new System.Windows.Forms.ListBox();
            this.lblMessageQuestion = new System.Windows.Forms.Label();
            this.lblMessageAnswers = new System.Windows.Forms.Label();
            this.txtBoxAnswer = new System.Windows.Forms.TextBox();
            this.btnCreateEditQuestion = new System.Windows.Forms.Button();
            this.chckBoxCorrectAnswer = new System.Windows.Forms.CheckBox();
            this.btnAddAnswer = new System.Windows.Forms.Button();
            this.lblMessageAnswer = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnEditAnswer = new System.Windows.Forms.Button();
            this.btnDeleteAnswer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtBoxQuestion
            // 
            this.txtBoxQuestion.BackColor = System.Drawing.SystemColors.Window;
            this.txtBoxQuestion.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoxQuestion.Location = new System.Drawing.Point(70, 53);
            this.txtBoxQuestion.Multiline = true;
            this.txtBoxQuestion.Name = "txtBoxQuestion";
            this.txtBoxQuestion.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtBoxQuestion.Size = new System.Drawing.Size(462, 102);
            this.txtBoxQuestion.TabIndex = 1;
            // 
            // lstBoxAnswers
            // 
            this.lstBoxAnswers.BackColor = System.Drawing.Color.LavenderBlush;
            this.lstBoxAnswers.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstBoxAnswers.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lstBoxAnswers.FormattingEnabled = true;
            this.lstBoxAnswers.ItemHeight = 21;
            this.lstBoxAnswers.Location = new System.Drawing.Point(70, 204);
            this.lstBoxAnswers.Name = "lstBoxAnswers";
            this.lstBoxAnswers.Size = new System.Drawing.Size(205, 151);
            this.lstBoxAnswers.TabIndex = 6;
            this.lstBoxAnswers.SelectedIndexChanged += new System.EventHandler(this.lstBoxAnswers_SelectedIndexChanged);
            // 
            // lblMessageQuestion
            // 
            this.lblMessageQuestion.AutoSize = true;
            this.lblMessageQuestion.Location = new System.Drawing.Point(65, 20);
            this.lblMessageQuestion.Name = "lblMessageQuestion";
            this.lblMessageQuestion.Size = new System.Drawing.Size(107, 30);
            this.lblMessageQuestion.TabIndex = 7;
            this.lblMessageQuestion.Text = "Прашање";
            // 
            // lblMessageAnswers
            // 
            this.lblMessageAnswers.AutoSize = true;
            this.lblMessageAnswers.Location = new System.Drawing.Point(65, 171);
            this.lblMessageAnswers.Name = "lblMessageAnswers";
            this.lblMessageAnswers.Size = new System.Drawing.Size(107, 30);
            this.lblMessageAnswers.TabIndex = 8;
            this.lblMessageAnswers.Text = "Одговори";
            // 
            // txtBoxAnswer
            // 
            this.txtBoxAnswer.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBoxAnswer.Location = new System.Drawing.Point(281, 228);
            this.txtBoxAnswer.Name = "txtBoxAnswer";
            this.txtBoxAnswer.Size = new System.Drawing.Size(251, 29);
            this.txtBoxAnswer.TabIndex = 9;
            // 
            // btnCreateEditQuestion
            // 
            this.btnCreateEditQuestion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCreateEditQuestion.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateEditQuestion.ForeColor = System.Drawing.SystemColors.GrayText;
            this.btnCreateEditQuestion.Location = new System.Drawing.Point(70, 362);
            this.btnCreateEditQuestion.Name = "btnCreateEditQuestion";
            this.btnCreateEditQuestion.Size = new System.Drawing.Size(102, 40);
            this.btnCreateEditQuestion.TabIndex = 25;
            this.btnCreateEditQuestion.Text = "<Креирај/Зачувај прашање>";
            this.btnCreateEditQuestion.UseVisualStyleBackColor = true;
            this.btnCreateEditQuestion.Click += new System.EventHandler(this.btnCreateEditQuestion_Click);
            // 
            // chckBoxCorrectAnswer
            // 
            this.chckBoxCorrectAnswer.AutoSize = true;
            this.chckBoxCorrectAnswer.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckBoxCorrectAnswer.Location = new System.Drawing.Point(285, 269);
            this.chckBoxCorrectAnswer.Name = "chckBoxCorrectAnswer";
            this.chckBoxCorrectAnswer.Size = new System.Drawing.Size(95, 25);
            this.chckBoxCorrectAnswer.TabIndex = 26;
            this.chckBoxCorrectAnswer.Text = "коректен";
            this.chckBoxCorrectAnswer.UseVisualStyleBackColor = true;
            // 
            // btnAddAnswer
            // 
            this.btnAddAnswer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddAnswer.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddAnswer.ForeColor = System.Drawing.SystemColors.GrayText;
            this.btnAddAnswer.Location = new System.Drawing.Point(285, 300);
            this.btnAddAnswer.Name = "btnAddAnswer";
            this.btnAddAnswer.Size = new System.Drawing.Size(68, 40);
            this.btnAddAnswer.TabIndex = 27;
            this.btnAddAnswer.Text = "Додај";
            this.btnAddAnswer.UseVisualStyleBackColor = true;
            this.btnAddAnswer.Click += new System.EventHandler(this.btnAddAnswer_Click);
            // 
            // lblMessageAnswer
            // 
            this.lblMessageAnswer.AutoSize = true;
            this.lblMessageAnswer.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessageAnswer.Location = new System.Drawing.Point(281, 204);
            this.lblMessageAnswer.Name = "lblMessageAnswer";
            this.lblMessageAnswer.Size = new System.Drawing.Size(72, 21);
            this.lblMessageAnswer.TabIndex = 28;
            this.lblMessageAnswer.Text = "Одговор";
            // 
            // btnCancel
            // 
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.ForeColor = System.Drawing.SystemColors.GrayText;
            this.btnCancel.Location = new System.Drawing.Point(173, 362);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(102, 40);
            this.btnCancel.TabIndex = 29;
            this.btnCancel.Text = "Отфрли";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnEditAnswer
            // 
            this.btnEditAnswer.Enabled = false;
            this.btnEditAnswer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEditAnswer.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditAnswer.ForeColor = System.Drawing.SystemColors.GrayText;
            this.btnEditAnswer.Location = new System.Drawing.Point(376, 300);
            this.btnEditAnswer.Name = "btnEditAnswer";
            this.btnEditAnswer.Size = new System.Drawing.Size(68, 40);
            this.btnEditAnswer.TabIndex = 30;
            this.btnEditAnswer.Text = "Уреди";
            this.btnEditAnswer.UseVisualStyleBackColor = true;
            this.btnEditAnswer.Click += new System.EventHandler(this.btnEditAnswer_Click);
            // 
            // btnDeleteAnswer
            // 
            this.btnDeleteAnswer.Enabled = false;
            this.btnDeleteAnswer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeleteAnswer.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteAnswer.ForeColor = System.Drawing.SystemColors.GrayText;
            this.btnDeleteAnswer.Location = new System.Drawing.Point(464, 300);
            this.btnDeleteAnswer.Name = "btnDeleteAnswer";
            this.btnDeleteAnswer.Size = new System.Drawing.Size(68, 40);
            this.btnDeleteAnswer.TabIndex = 31;
            this.btnDeleteAnswer.Text = "Избриши";
            this.btnDeleteAnswer.UseVisualStyleBackColor = true;
            this.btnDeleteAnswer.Click += new System.EventHandler(this.btnDeleteAnswer_Click);
            // 
            // FormProfesorCreateEditQuestion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.ClientSize = new System.Drawing.Size(544, 422);
            this.Controls.Add(this.btnDeleteAnswer);
            this.Controls.Add(this.btnEditAnswer);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.lblMessageAnswer);
            this.Controls.Add(this.btnAddAnswer);
            this.Controls.Add(this.chckBoxCorrectAnswer);
            this.Controls.Add(this.btnCreateEditQuestion);
            this.Controls.Add(this.txtBoxAnswer);
            this.Controls.Add(this.lblMessageAnswers);
            this.Controls.Add(this.lblMessageQuestion);
            this.Controls.Add(this.lstBoxAnswers);
            this.Controls.Add(this.txtBoxQuestion);
            this.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.GrayText;
            this.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.Name = "FormProfesorCreateEditQuestion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "<OnlineKolokvium - Професор - Креирај/Уреди прашање>";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormProfesorCreateEditQuestion_FormClosing);
            this.Load += new System.EventHandler(this.FormProfesorCreateEditQuestion_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtBoxQuestion;
        private System.Windows.Forms.ListBox lstBoxAnswers;
        private System.Windows.Forms.Label lblMessageQuestion;
        private System.Windows.Forms.Label lblMessageAnswers;
        private System.Windows.Forms.TextBox txtBoxAnswer;
        private System.Windows.Forms.Button btnCreateEditQuestion;
        private System.Windows.Forms.CheckBox chckBoxCorrectAnswer;
        private System.Windows.Forms.Button btnAddAnswer;
        private System.Windows.Forms.Label lblMessageAnswer;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnEditAnswer;
        private System.Windows.Forms.Button btnDeleteAnswer;
    }
}