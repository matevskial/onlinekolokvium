﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OnlineKolokviumLibrary;
using OnlineKolokviumLibrary.Models;

namespace OnlineKolokviumUI
{
    public partial class FormStudentLogin : Form
    {
        FormMain frmMain;

        public FormStudentLogin(FormMain frmMain)
        {
            InitializeComponent();
            this.frmMain = frmMain;
            this.AcceptButton = btnLogin;
        }

        private void mnuStripHome_Click(object sender, EventArgs e)
        {
            frmMain.Show();
            this.Close();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            StudentModel s = ProgramHelper.Db.PerformLoginStudent(txtBoxLoginId.Text, txtBoxLoginPassword.Text);
            if(s == null)
            {
                MessageBox.Show("Невалидна најава поради грешно корисничко име или лозинка!", "Невалидна најава", MessageBoxButtons.OK, MessageBoxIcon.Error); 
            }
            else
            {
                FormStudentDashboard frmStudentDashboard = new FormStudentDashboard(frmMain, s);
                frmStudentDashboard.Show();
                this.Close();
            }
        }
    }
}
