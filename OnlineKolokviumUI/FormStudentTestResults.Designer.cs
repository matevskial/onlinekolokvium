﻿namespace OnlineKolokviumUI
{
    partial class FormStudentTestResults
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpBoxResults = new System.Windows.Forms.GroupBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblUnansweredQuestions = new System.Windows.Forms.Label();
            this.lblMessageStatus = new System.Windows.Forms.Label();
            this.lblMessageUnansweredQuestions = new System.Windows.Forms.Label();
            this.lblWrongQuestions = new System.Windows.Forms.Label();
            this.lblMessageWrongQuestions = new System.Windows.Forms.Label();
            this.lblCorrectQuestions = new System.Windows.Forms.Label();
            this.lblMessageCorrectQuestions = new System.Windows.Forms.Label();
            this.grpBoxResults.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpBoxResults
            // 
            this.grpBoxResults.Controls.Add(this.lblStatus);
            this.grpBoxResults.Controls.Add(this.lblUnansweredQuestions);
            this.grpBoxResults.Controls.Add(this.lblMessageStatus);
            this.grpBoxResults.Controls.Add(this.lblMessageUnansweredQuestions);
            this.grpBoxResults.Controls.Add(this.lblWrongQuestions);
            this.grpBoxResults.Controls.Add(this.lblMessageWrongQuestions);
            this.grpBoxResults.Controls.Add(this.lblCorrectQuestions);
            this.grpBoxResults.Controls.Add(this.lblMessageCorrectQuestions);
            this.grpBoxResults.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpBoxResults.Location = new System.Drawing.Point(15, 12);
            this.grpBoxResults.Name = "grpBoxResults";
            this.grpBoxResults.Size = new System.Drawing.Size(230, 228);
            this.grpBoxResults.TabIndex = 15;
            this.grpBoxResults.TabStop = false;
            this.grpBoxResults.Text = "<TestName>";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(71, 161);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(50, 21);
            this.lblStatus.TabIndex = 14;
            this.lblStatus.Text = "<str>";
            // 
            // lblUnansweredQuestions
            // 
            this.lblUnansweredQuestions.AutoSize = true;
            this.lblUnansweredQuestions.Location = new System.Drawing.Point(128, 125);
            this.lblUnansweredQuestions.Name = "lblUnansweredQuestions";
            this.lblUnansweredQuestions.Size = new System.Drawing.Size(50, 21);
            this.lblUnansweredQuestions.TabIndex = 13;
            this.lblUnansweredQuestions.Text = "<23>";
            // 
            // lblMessageStatus
            // 
            this.lblMessageStatus.AutoSize = true;
            this.lblMessageStatus.Location = new System.Drawing.Point(5, 161);
            this.lblMessageStatus.Name = "lblMessageStatus";
            this.lblMessageStatus.Size = new System.Drawing.Size(60, 21);
            this.lblMessageStatus.TabIndex = 12;
            this.lblMessageStatus.Text = "Статус:";
            // 
            // lblMessageUnansweredQuestions
            // 
            this.lblMessageUnansweredQuestions.AutoSize = true;
            this.lblMessageUnansweredQuestions.Location = new System.Drawing.Point(5, 125);
            this.lblMessageUnansweredQuestions.Name = "lblMessageUnansweredQuestions";
            this.lblMessageUnansweredQuestions.Size = new System.Drawing.Size(117, 21);
            this.lblMessageUnansweredQuestions.TabIndex = 11;
            this.lblMessageUnansweredQuestions.Text = "Неодговорени:";
            // 
            // lblWrongQuestions
            // 
            this.lblWrongQuestions.AutoSize = true;
            this.lblWrongQuestions.Location = new System.Drawing.Point(175, 90);
            this.lblWrongQuestions.Name = "lblWrongQuestions";
            this.lblWrongQuestions.Size = new System.Drawing.Size(50, 21);
            this.lblWrongQuestions.TabIndex = 10;
            this.lblWrongQuestions.Text = "<23>";
            // 
            // lblMessageWrongQuestions
            // 
            this.lblMessageWrongQuestions.AutoSize = true;
            this.lblMessageWrongQuestions.Location = new System.Drawing.Point(5, 90);
            this.lblMessageWrongQuestions.Name = "lblMessageWrongQuestions";
            this.lblMessageWrongQuestions.Size = new System.Drawing.Size(164, 21);
            this.lblMessageWrongQuestions.TabIndex = 9;
            this.lblMessageWrongQuestions.Text = "Неточно одговорени:";
            // 
            // lblCorrectQuestions
            // 
            this.lblCorrectQuestions.AutoSize = true;
            this.lblCorrectQuestions.Location = new System.Drawing.Point(157, 50);
            this.lblCorrectQuestions.Name = "lblCorrectQuestions";
            this.lblCorrectQuestions.Size = new System.Drawing.Size(50, 21);
            this.lblCorrectQuestions.TabIndex = 8;
            this.lblCorrectQuestions.Text = "<23>";
            // 
            // lblMessageCorrectQuestions
            // 
            this.lblMessageCorrectQuestions.AutoSize = true;
            this.lblMessageCorrectQuestions.Location = new System.Drawing.Point(5, 50);
            this.lblMessageCorrectQuestions.Name = "lblMessageCorrectQuestions";
            this.lblMessageCorrectQuestions.Size = new System.Drawing.Size(146, 21);
            this.lblMessageCorrectQuestions.TabIndex = 7;
            this.lblMessageCorrectQuestions.Text = "Точно одговорени:";
            // 
            // FormStudentTestResults
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.ClientSize = new System.Drawing.Size(254, 252);
            this.Controls.Add(this.grpBoxResults);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.GrayText;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FormStudentTestResults";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "<NameTest>";
            this.Load += new System.EventHandler(this.FormStudentTestResults_Load);
            this.grpBoxResults.ResumeLayout(false);
            this.grpBoxResults.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpBoxResults;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblUnansweredQuestions;
        private System.Windows.Forms.Label lblMessageStatus;
        private System.Windows.Forms.Label lblMessageUnansweredQuestions;
        private System.Windows.Forms.Label lblWrongQuestions;
        private System.Windows.Forms.Label lblMessageWrongQuestions;
        private System.Windows.Forms.Label lblCorrectQuestions;
        private System.Windows.Forms.Label lblMessageCorrectQuestions;
    }
}