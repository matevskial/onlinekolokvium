﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OnlineKolokviumLibrary;
using OnlineKolokviumLibrary.Models;

namespace OnlineKolokviumUI
{
    public partial class FormProfesorLogin : Form
    {
        FormMain frmMain;

        public FormProfesorLogin(FormMain frmMain)
        {
            InitializeComponent();
            this.frmMain = frmMain;
            this.AcceptButton = btnLogin;
        }

        private void mnuStripHome_Click(object sender, EventArgs e)
        {
            frmMain.Show();
            this.Close();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            ProfesorModel p = ProgramHelper.Db.PerformLoginProfesor(txtBoxLoginId.Text, txtBoxLoginPassword.Text);
            if (p == null)
            {
                MessageBox.Show("Невалидна најава поради грешно корисничко име или лозинка!", "Невалидна најава", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                FormProfesorDashboard frmProfesorDashboard = new FormProfesorDashboard(frmMain, p);
                frmProfesorDashboard.Show();
                this.Close();
            }
        }
    }
}
