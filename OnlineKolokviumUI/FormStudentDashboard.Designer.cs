﻿namespace OnlineKolokviumUI
{
    partial class FormStudentDashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstBoxTakenTests = new System.Windows.Forms.ListBox();
            this.btnResults = new System.Windows.Forms.Button();
            this.lstBoxUntakenTests = new System.Windows.Forms.ListBox();
            this.btnTakeExam = new System.Windows.Forms.Button();
            this.lblMessageExamTakenSubjects = new System.Windows.Forms.Label();
            this.lblMessageExamsToTake = new System.Windows.Forms.Label();
            this.cmbBoxSession = new System.Windows.Forms.ComboBox();
            this.mnuStrip = new System.Windows.Forms.MenuStrip();
            this.mnuStripLogout = new System.Windows.Forms.ToolStripMenuItem();
            this.chckBoxPassedSubjects = new System.Windows.Forms.CheckBox();
            this.lblStudentInfo = new System.Windows.Forms.Label();
            this.mnuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstBoxTakenTests
            // 
            this.lstBoxTakenTests.BackColor = System.Drawing.Color.LavenderBlush;
            this.lstBoxTakenTests.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstBoxTakenTests.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lstBoxTakenTests.FormattingEnabled = true;
            this.lstBoxTakenTests.ItemHeight = 21;
            this.lstBoxTakenTests.Location = new System.Drawing.Point(53, 166);
            this.lstBoxTakenTests.Name = "lstBoxTakenTests";
            this.lstBoxTakenTests.Size = new System.Drawing.Size(193, 193);
            this.lstBoxTakenTests.TabIndex = 0;
            this.lstBoxTakenTests.SelectedIndexChanged += new System.EventHandler(this.lstBoxTakenTests_SelectedIndexChanged);
            // 
            // btnResults
            // 
            this.btnResults.Enabled = false;
            this.btnResults.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnResults.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResults.ForeColor = System.Drawing.SystemColors.GrayText;
            this.btnResults.Location = new System.Drawing.Point(53, 371);
            this.btnResults.Name = "btnResults";
            this.btnResults.Size = new System.Drawing.Size(193, 40);
            this.btnResults.TabIndex = 7;
            this.btnResults.Text = "Види резултати";
            this.btnResults.UseVisualStyleBackColor = true;
            this.btnResults.Click += new System.EventHandler(this.btnResults_Click);
            // 
            // lstBoxUntakenTests
            // 
            this.lstBoxUntakenTests.BackColor = System.Drawing.Color.LavenderBlush;
            this.lstBoxUntakenTests.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstBoxUntakenTests.ForeColor = System.Drawing.SystemColors.GrayText;
            this.lstBoxUntakenTests.FormattingEnabled = true;
            this.lstBoxUntakenTests.ItemHeight = 21;
            this.lstBoxUntakenTests.Location = new System.Drawing.Point(303, 105);
            this.lstBoxUntakenTests.Name = "lstBoxUntakenTests";
            this.lstBoxUntakenTests.Size = new System.Drawing.Size(193, 256);
            this.lstBoxUntakenTests.TabIndex = 8;
            this.lstBoxUntakenTests.SelectedIndexChanged += new System.EventHandler(this.lstBoxUntakenTests_SelectedIndexChanged);
            // 
            // btnTakeExam
            // 
            this.btnTakeExam.Enabled = false;
            this.btnTakeExam.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTakeExam.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTakeExam.ForeColor = System.Drawing.SystemColors.GrayText;
            this.btnTakeExam.Location = new System.Drawing.Point(303, 371);
            this.btnTakeExam.Name = "btnTakeExam";
            this.btnTakeExam.Size = new System.Drawing.Size(193, 40);
            this.btnTakeExam.TabIndex = 9;
            this.btnTakeExam.Text = "Полагај";
            this.btnTakeExam.UseVisualStyleBackColor = true;
            this.btnTakeExam.Click += new System.EventHandler(this.btnTakeExam_Click);
            // 
            // lblMessageExamTakenSubjects
            // 
            this.lblMessageExamTakenSubjects.AutoSize = true;
            this.lblMessageExamTakenSubjects.Location = new System.Drawing.Point(48, 58);
            this.lblMessageExamTakenSubjects.Name = "lblMessageExamTakenSubjects";
            this.lblMessageExamTakenSubjects.Size = new System.Drawing.Size(185, 30);
            this.lblMessageExamTakenSubjects.TabIndex = 10;
            this.lblMessageExamTakenSubjects.Text = "Полагани тестови";
            // 
            // lblMessageExamsToTake
            // 
            this.lblMessageExamsToTake.AutoSize = true;
            this.lblMessageExamsToTake.Location = new System.Drawing.Point(298, 58);
            this.lblMessageExamsToTake.Name = "lblMessageExamsToTake";
            this.lblMessageExamsToTake.Size = new System.Drawing.Size(134, 30);
            this.lblMessageExamsToTake.TabIndex = 11;
            this.lblMessageExamsToTake.Text = "За полагање";
            // 
            // cmbBoxSession
            // 
            this.cmbBoxSession.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoxSession.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBoxSession.FormattingEnabled = true;
            this.cmbBoxSession.Location = new System.Drawing.Point(53, 100);
            this.cmbBoxSession.Name = "cmbBoxSession";
            this.cmbBoxSession.Size = new System.Drawing.Size(193, 29);
            this.cmbBoxSession.TabIndex = 12;
            this.cmbBoxSession.SelectedIndexChanged += new System.EventHandler(this.cmbBoxSession_SelectedIndexChanged);
            // 
            // mnuStrip
            // 
            this.mnuStrip.BackColor = System.Drawing.Color.Lavender;
            this.mnuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuStripLogout});
            this.mnuStrip.Location = new System.Drawing.Point(0, 0);
            this.mnuStrip.Name = "mnuStrip";
            this.mnuStrip.Size = new System.Drawing.Size(544, 24);
            this.mnuStrip.TabIndex = 13;
            this.mnuStrip.Text = "menuStrip1";
            // 
            // mnuStripLogout
            // 
            this.mnuStripLogout.Name = "mnuStripLogout";
            this.mnuStripLogout.Size = new System.Drawing.Size(71, 20);
            this.mnuStripLogout.Text = "Одјави се";
            this.mnuStripLogout.Click += new System.EventHandler(this.mnuStripLogout_Click);
            // 
            // chckBoxPassedSubjects
            // 
            this.chckBoxPassedSubjects.AutoSize = true;
            this.chckBoxPassedSubjects.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chckBoxPassedSubjects.Location = new System.Drawing.Point(53, 135);
            this.chckBoxPassedSubjects.Name = "chckBoxPassedSubjects";
            this.chckBoxPassedSubjects.Size = new System.Drawing.Size(200, 25);
            this.chckBoxPassedSubjects.TabIndex = 14;
            this.chckBoxPassedSubjects.Text = "само положени тестови";
            this.chckBoxPassedSubjects.UseVisualStyleBackColor = true;
            this.chckBoxPassedSubjects.CheckedChanged += new System.EventHandler(this.chckBoxPassedSubjects_CheckedChanged);
            // 
            // lblStudentInfo
            // 
            this.lblStudentInfo.AutoSize = true;
            this.lblStudentInfo.Location = new System.Drawing.Point(48, 24);
            this.lblStudentInfo.Name = "lblStudentInfo";
            this.lblStudentInfo.Size = new System.Drawing.Size(150, 30);
            this.lblStudentInfo.TabIndex = 15;
            this.lblStudentInfo.Text = "<StudentInfo>";
            // 
            // FormStudentDashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.ClientSize = new System.Drawing.Size(544, 422);
            this.Controls.Add(this.lblStudentInfo);
            this.Controls.Add(this.chckBoxPassedSubjects);
            this.Controls.Add(this.cmbBoxSession);
            this.Controls.Add(this.lblMessageExamsToTake);
            this.Controls.Add(this.lblMessageExamTakenSubjects);
            this.Controls.Add(this.btnTakeExam);
            this.Controls.Add(this.lstBoxUntakenTests);
            this.Controls.Add(this.btnResults);
            this.Controls.Add(this.lstBoxTakenTests);
            this.Controls.Add(this.mnuStrip);
            this.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.GrayText;
            this.MainMenuStrip = this.mnuStrip;
            this.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.Name = "FormStudentDashboard";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OnlineKolovkium - Студент - Почетна";
            this.Load += new System.EventHandler(this.FormStudentDashboard_Load);
            this.mnuStrip.ResumeLayout(false);
            this.mnuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstBoxTakenTests;
        private System.Windows.Forms.Button btnResults;
        private System.Windows.Forms.ListBox lstBoxUntakenTests;
        private System.Windows.Forms.Button btnTakeExam;
        private System.Windows.Forms.Label lblMessageExamTakenSubjects;
        private System.Windows.Forms.Label lblMessageExamsToTake;
        private System.Windows.Forms.ComboBox cmbBoxSession;
        private System.Windows.Forms.MenuStrip mnuStrip;
        private System.Windows.Forms.ToolStripMenuItem mnuStripLogout;
        private System.Windows.Forms.CheckBox chckBoxPassedSubjects;
        private System.Windows.Forms.Label lblStudentInfo;
    }
}