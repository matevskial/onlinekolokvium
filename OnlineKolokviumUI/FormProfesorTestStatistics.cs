﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OnlineKolokviumLibrary;
using OnlineKolokviumLibrary.Models;

namespace OnlineKolokviumUI
{
    public partial class FormProfesorTestStatistics : Form
    {
        TestModel test;

        List<TestResultModel> results;
        /// <summary>
        /// Students that took the TestModel test
        /// </summary>
        List<StudentModel> students;
        TestStatisticsModel statistics;

        List<StudentModel> filteredStudents;
        TestResultModel selectedResult;

        public FormProfesorTestStatistics(TestModel test)
        {
            InitializeComponent();
            this.test = test;
        }

        private void FormProfesorTestStatistics_Load(object sender, EventArgs e)
        {
            GetData();
            FilterData();
            FillData();
        }

        private void lstBoxStudents_Format(object sender, ListControlConvertEventArgs e)
        {
            string FirstName = ((StudentModel)e.ListItem).FirstName;
            string LastName = ((StudentModel)e.ListItem).LastName;
            string Index = ((StudentModel)e.ListItem).Index;
            e.Value = FirstName + " " + LastName + " " + Index;
        }

        private void chckBoxStudentsPassed_CheckedChanged(object sender, EventArgs e)
        {
            FilterStudents();
            FillLstBoxStudents();
        }

        private void lstBoxStudents_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstBoxStudents.SelectedIndex >= 0)
            {
                FilterSelectedResult();
                FillGrpBoxStudentInfo();
            }
            else
            {
                ClearGrpBoxStudentInfo();
            }
        }

        private void GetData()
        {
            GetResults();
            GetStudents();
            GetStatistics();
        }

        private void GetResults()
        {
            results = ProgramHelper.Db.GetTestResultsByTest(test.ID);
        }

        private void GetStudents()
        {
            students = ProgramHelper.Db.GetStudentsByTakenTest(test.ID);
        }

        private void GetStatistics()
        {
            statistics = ProgramHelper.Db.GetStatisticsForTest(test.ID);
        }

        private void FilterData()
        {
            FilterStudents();
        }

        private void FilterStudents()
        {
            filteredStudents = new List<StudentModel>();
            bool shouldFilterByPassed = chckBoxStudentsPassed.Checked;

            for(int i = 0; i < results.Count; i++)
            {
                if (shouldFilterByPassed)
                {
                    if (results[i].IsPassed)
                        filteredStudents.Add(results[i].student);
                }
                else
                {
                    filteredStudents.Add(results[i].student);
                }
            }
        }

        private void FilterSelectedResult()
        {
            StudentModel student = ((StudentModel)lstBoxStudents.SelectedItem);
            
            for (int i = 0; i < results.Count; i++)
            {
                if(results[i].student.ID == student.ID)
                {
                    selectedResult = results[i];
                    break;
                }
            }
        }

        private void FillData()
        {
            FillTestInfo();
            FillLstBoxStudents();
            FillGrpBoxStudentInfo();
            FillGrpBoxStatistics();
        }

        private void FillTestInfo()
        {
            this.Text = "OnlineKolokvium - Професор - " + test.Name + " - Статистика";
            lblTestName.Text = test.Name;
        }

        private void FillLstBoxStudents()
        {
            
            lstBoxStudents.DataSource = null;
            lstBoxStudents.DataSource = filteredStudents;
            lstBoxStudents.DisplayMember = "FirstName";
            lstBoxStudents.ClearSelected();
        }

        private void FillGrpBoxStudentInfo()
        {
            if (selectedResult == null) return;
            grpBoxStudentInfo.Text = selectedResult.student.FirstName + " " + selectedResult.student.LastName + " " + selectedResult.student.Index;
            lblCorrectQuestions.Text = selectedResult.CorrectAnswers.ToString();
            lblWrongQuestions.Text = selectedResult.WrongAnswers.ToString();
            lblUnansweredQuestions.Text = selectedResult.QuestionsUnanswered.ToString();
            if (selectedResult.IsPassed)
            {
                lblStatus.Text = "Положен";
                lblStatus.ForeColor = Color.Green;
            }
            else
            {
                lblStatus.Text = "Неположен";
                lblStatus.ForeColor = Color.Red;
            }
        }

        private void FillGrpBoxStatistics()
        {
            if (statistics == null)
            {
                Console.WriteLine("statistics in FillGrpBox is null");
            }
            lblStudentsNumber.Text = statistics.StudentsCount.ToString();
            lblStudentsPassed.Text = statistics.StudentsPassed.ToString();
            lblStudentsFailed.Text = statistics.StudentsFailed.ToString();
        }

        private void ClearGrpBoxStudentInfo()
        {
            grpBoxStudentInfo.Text = "";
            lblCorrectQuestions.Text = "";
            lblWrongQuestions.Text = "";
            lblUnansweredQuestions.Text = "";
            lblStatus.Text = "";
        }
    }
}
