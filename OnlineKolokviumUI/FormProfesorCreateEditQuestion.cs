﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OnlineKolokviumLibrary;
using OnlineKolokviumLibrary.Models;

namespace OnlineKolokviumUI
{
    public partial class FormProfesorCreateEditQuestion : Form
    {
        bool IsCreateMode;
        bool IsBtnCancelClicked = false;
        bool IsBtnCreateEditClicked = false;

        public QuestionModel question;
        int correctAnswers;
        public FormProfesorCreateEditQuestion()
        {
            InitializeComponent();
            IsCreateMode = true;
            question = new QuestionModel(-1, -1, "");
            correctAnswers = 0;
            this.Text = "OnlineKolokvium - Професор - Креирај прашање";
        }

        public FormProfesorCreateEditQuestion(QuestionModel question)
        {
            InitializeComponent();
            IsCreateMode = false;
            this.question = question;
            correctAnswers = 1;
            this.Text = "OnlineKolokvium - Професор - Уреди прашање";
        }

        private void FormProfesorCreateEditQuestion_Load(object sender, EventArgs e)
        {
            
            if(IsCreateMode)
            {
                btnCreateEditQuestion.Text = "Креирај";
            }
            else
            {
                btnCreateEditQuestion.Text = "Зачувај";
                FillData();
            }
        }

        private void FillData()
        {
            txtBoxQuestion.Text = question.Text;
            FillLstBoxAnswers();
        }

        private void FillLstBoxAnswers()
        {
            lstBoxAnswers.DataSource = null;
            lstBoxAnswers.DataSource = question.Answers;
            lstBoxAnswers.DisplayMember = "Text";
            lstBoxAnswers.ClearSelected();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Промените нема да се зачуваат. Дали сакате да продолжите?", "Потврда", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {
                IsBtnCancelClicked = true;
                this.Close();
            }
            else
            {
                IsBtnCancelClicked = false;
            }
        }

        private void FormProfesorCreateEditQuestion_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsBtnCreateEditClicked)
            {
                if (MessageBox.Show("Дали сакате овие промени да се зачуваат?", "Потврда", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.Cancel)
                {
                    IsBtnCreateEditClicked = false;
                    e.Cancel = true;
                }
                UpdateQuestion();
                //UpdateAnswers();
                DialogResult = DialogResult.OK;
            }
            else if (!IsBtnCancelClicked)
            {
                if (MessageBox.Show("Промените нема да се зачуваат. Дали сакате да продолжите?", "Потврда", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.Cancel)
                {
                    e.Cancel = true;
                }
                DialogResult = DialogResult.Cancel;
            }
            else
            {
                DialogResult = DialogResult.Cancel;
            }
        }

        
        private void UpdateQuestion()
        {
            question.Text = txtBoxQuestion.Text;
        }

        private void btnCreateEditQuestion_Click(object sender, EventArgs e)
        {
            if (ValidateData())
            {
                IsBtnCreateEditClicked = true;
                this.Close();
            }
            else
            {
                MessageBox.Show("Имате внесено невалидни податоци за некои полиња.", "Грешка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private bool ValidateData()
        {
            if (txtBoxQuestion.Text == "" || txtBoxQuestion.Text == null)
                return false;
            if (correctAnswers == 0)
                return false;
            return true;
                
        }

        private bool ValidateAnswerData()
        {
            if (correctAnswers == 1 && chckBoxCorrectAnswer.Checked)
                return false;
            if (txtBoxAnswer.Text == "" || txtBoxAnswer == null)
                return false;
            return true;
        }

        

        private void btnAddAnswer_Click(object sender, EventArgs e)
        {
            if(ValidateAnswerData())
            {
                AnswerModel a = new AnswerModel(-1, -1, txtBoxAnswer.Text, chckBoxCorrectAnswer.Checked);
                if (chckBoxCorrectAnswer.Checked) correctAnswers++;
                question.Answers.Add(a);
                FillLstBoxAnswers();
            }
            else
            {
                MessageBox.Show("Невалидни податоци за одговорот", "Грешка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnEditAnswer_Click(object sender, EventArgs e)
        {
            AnswerModel sa = ((AnswerModel)lstBoxAnswers.SelectedItem);
            if( (!sa.IsCorrect && chckBoxCorrectAnswer.Checked && correctAnswers == 1) || txtBoxAnswer.Text == "" || txtBoxAnswer.Text == null)
            {
                MessageBox.Show("Невалидни податоци за одговорот", "Грешка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                sa.Text = txtBoxAnswer.Text;

                if (sa.IsCorrect && !chckBoxCorrectAnswer.Checked) correctAnswers--;
                else if (!sa.IsCorrect && chckBoxCorrectAnswer.Checked) correctAnswers++;

                sa.IsCorrect = chckBoxCorrectAnswer.Checked;
                FillLstBoxAnswers();
            }
            
        }

        private void lstBoxAnswers_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstBoxAnswers.SelectedIndex >= 0)
            {
                btnDeleteAnswer.Enabled = true;
                btnEditAnswer.Enabled = true;
                AnswerModel sa = ((AnswerModel)lstBoxAnswers.SelectedItem);
                txtBoxAnswer.Text = sa.Text;
                chckBoxCorrectAnswer.Checked = sa.IsCorrect;
            }
            else
            {
                btnDeleteAnswer.Enabled = false;
                btnEditAnswer.Enabled = false;
                txtBoxAnswer.Text = "";
                chckBoxCorrectAnswer.Checked = false;
            }
        }

        private void btnDeleteAnswer_Click(object sender, EventArgs e)
        {
            AnswerModel doomed = ((AnswerModel)lstBoxAnswers.SelectedItem);
            question.Answers.Remove(doomed);
            FillLstBoxAnswers();
        }
    }
}
