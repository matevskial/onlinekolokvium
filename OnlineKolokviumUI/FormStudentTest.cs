﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using OnlineKolokviumLibrary;
using OnlineKolokviumLibrary.Models;

namespace OnlineKolokviumUI
{
    public partial class FormStudentTest : Form
    {
        Form frmParent;
        StudentModel student;
        TestModel test;
        int currQuestion;
        CountDown cd;

        bool IsTimeElapsed;
        bool IsCanceled = false;

        List<QuestionModel> questions;

        List<AnswerModel> answers;
        List<List<RadioButton>> radioButtons;

        public FormStudentTest()
        {
            InitializeComponent();
        }

        public FormStudentTest(Form frmParent, StudentModel student, TestModel test)
        {
            InitializeComponent();
            this.frmParent = frmParent;
            this.student = student;
            this.test = test;
        }

        private void FormStudentTest_Load(object sender, EventArgs e)
        {
            GetData();
            FilterData();
            FillData();
            StartTest();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            lstViewQuestions.Items[currQuestion+1].Selected = true;
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            lstViewQuestions.Items[currQuestion-1].Selected = true;
        }

        private void lstViewQuestions_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstViewQuestions.SelectedIndices.Count > 0)
            {
                ChangeQuestion(lstViewQuestions.SelectedIndices[0]);
            }
        }

        private void btnEnd_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormStudentTest_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(IsTimeElapsed)
            {
                MessageBox.Show("Тестот заврши, ве молиме притиснете ОК за да го затворите успешно", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                CreateResults();
                this.DialogResult = DialogResult.OK;
            }
            else if(!IsCanceled)
            {
                if (MessageBox.Show("Дали сте сигурни дека сакате да завршите со тестирањето?", "Потврда", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                {
                    CreateResults();
                    cd.Stop();
                    this.DialogResult = DialogResult.OK;
                }
                else
                {
                    e.Cancel = true;
                }
            }
            else
            {
                this.DialogResult = DialogResult.OK;
            }
        }

        private void radioButton_CheckedChanged(object sender, EventArgs e)
        {
            RecordAnswer();
        }

        private void OnTimeEvent(object sender, EventArgs e)
        {
            cd.Tick();
            if (cd.Ticks >= cd.TotalSeconds)
            {
                IsTimeElapsed = true;
                cd.Stop();
                this.Close();
            }
            else
            {
                lblTimer.Text = cd.Display();
            }
        }

        private void ShowSplash()
        {
            if(new FormStudentTestInitialSplash(test).ShowDialog() == DialogResult.Cancel)
            {
                IsCanceled = true;
                this.Close();
            }
        }

        private void GetData()
        {
            GetQuestionsAndAnswers();
        }

        private void GetQuestionsAndAnswers()
        {
            questions = ProgramHelper.Db.GetQuestionsByTest(test.ID);
            test.Questions = questions;
        }

        private void FilterData()
        {
            
        }
        
        private void FillData()
        {
            FillStudentInfo();
            FillTestInfo();
        }

        private void FillStudentInfo()
        {
            lblStudentInfo.Text = student.FirstName + " " + student.LastName + " " + student.Index;
        }

        private void FillTestInfo()
        {
            lblTestName.Text = test.Name + " " + test.Date.ToShortDateString();
            this.Text = "OnlineKolokvium - Студент - Полагај - " + test.Name + " " + test.Date.ToShortDateString();
        }

        private void FillLstViewQuestions()
        {
            lstViewQuestions.View = View.Details;
            lstViewQuestions.Columns.Add(new ColumnHeader() { Text = "", Name = "col1" });
            lstViewQuestions.HeaderStyle = ColumnHeaderStyle.None;
            for (int i = 0; i < questions.Count; i++)
            {
                lstViewQuestions.Items.Add((i + 1).ToString() + "            ");
            }
        }

        private void FillGrpBoxQuestion()
        {
            if(currQuestion >= 0)
            {
                grpBoxQuestion.Text = "Прашање - " + (currQuestion + 1).ToString();

                flwLayoutAnswers.Controls.Clear();

                QuestionModel q = questions[currQuestion];
                txtBoxQuestion.Text = q.Text;

                for(int i = 0; i < radioButtons[currQuestion].Count; i++)
                {
                    flwLayoutAnswers.Controls.Add(radioButtons[currQuestion][i]);
                }
            }
        }

        private void StartTest()
        {
            ShowSplash();
            InitializeAnswers();
            FillLstViewQuestions();
            InitializeFirstQuestion();
            InitializeCountDown();
        }

        private void InitializeAnswers()
        {
            answers = new List<AnswerModel>();
            for(int i = 0; i < questions.Count; i++)
            {
                answers.Add(null);
            }

            radioButtons = new List<List<RadioButton>>();
            for(int i = 0; i < questions.Count; i++)
            {
                radioButtons.Add(new List<RadioButton>());
                for(int j = 0; j < questions[i].Answers.Count; j++)
                {
                    AnswerModel a = questions[i].Answers[j];
                    RadioButton r = new RadioButton() { Text = a.Text, Checked = false, Tag = a, AutoSize = true};
                    r.CheckedChanged += new EventHandler(this.radioButton_CheckedChanged);
                    radioButtons[i].Add(r);
                }
            }
        }

        private void InitializeCountDown()
        {
            IsTimeElapsed = false;
            cd = new CountDown(test.Duration, 0, OnTimeEvent);
            lblTimer.Text = cd.Display();
            cd.Start();
        }

        private void InitializeFirstQuestion()
        {
            currQuestion = -1;
            if (questions.Count > 0)
                currQuestion = 0;

            lstViewQuestions.Items[currQuestion].Selected = true;
        }

        private void SetButtonStates()
        {
            if(currQuestion == -1 || (questions.Count == 1))
            {
                btnNext.Enabled = false;
                btnPrevious.Enabled = false;
            }
            else if(currQuestion == 0)
            {
                btnPrevious.Enabled = false;
                btnNext.Enabled = true;
            }
            else if(currQuestion == questions.Count - 1)
            {
                btnPrevious.Enabled = true;
                btnNext.Enabled = false;
            }
            else
            {
                btnPrevious.Enabled = true;
                btnNext.Enabled = true;
            }
        }

        private void ChangeQuestion(int newQuestionIndex)
        {
            currQuestion = newQuestionIndex;
            SetButtonStates();
            FillGrpBoxQuestion();
        }
        private void RecordAnswer()
        {
            RadioButton r = null;
            for(int i = 0; i < flwLayoutAnswers.Controls.Count; i++)
            {
                if(((RadioButton)flwLayoutAnswers.Controls[i]).Checked)
                {
                    r = ((RadioButton)flwLayoutAnswers.Controls[i]);
                    break;
                }
            }
            if(r != null)
            {
                lstViewQuestions.Items[currQuestion].BackColor = Color.Thistle;
                answers[currQuestion] = ((AnswerModel)r.Tag);
            }
        }

        private void lstViewQuestions_DrawItem(object sender, DrawListViewItemEventArgs e)
        {
            e.DrawBackground();
            e.DrawText();
            if(e.Item.Selected)
            {
                e.Graphics.FillRectangle(Brushes.LightSteelBlue, e.Bounds);
                e.DrawFocusRectangle();
                e.DrawText();
            }
        }

        private void CreateResults()
        {
            TestResultModel results = new TestResultModel(-1, student.ID, test.ID, false, 0, 0, 0, 0);
            for(int i = 0; i < answers.Count; i++)
            {
                if (answers[i] == null)
                {
                    results.QuestionsUnanswered++;
                }
                else if(answers[i].IsCorrect)
                {
                    results.QuestionsAnswered++;
                    results.CorrectAnswers++;
                }
                else
                {
                    results.QuestionsAnswered++;
                    results.WrongAnswers++;
                }
            }
            if(results.CorrectAnswers > questions.Count / 2)
            {
                results.IsPassed = true;
            }

            ProgramHelper.Db.CreateResults(results);
        }
    }
}
