﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OnlineKolokviumLibrary;
using OnlineKolokviumLibrary.Models;

namespace OnlineKolokviumUI
{
    public partial class FormStudentTestResults : Form
    {
        TestResultModel r;
        public FormStudentTestResults(TestResultModel r)
        {
            InitializeComponent();
            this.r = r;
        }

        private void FormStudentTestResults_Load(object sender, EventArgs e)
        {
            FillData();
        }

        private void FillData()
        {
            this.Text = r.test.Name + "- резултати";
            grpBoxResults.Text = r.test.Name;
            lblCorrectQuestions.Text = r.CorrectAnswers.ToString();
            lblWrongQuestions.Text = r.WrongAnswers.ToString();
            lblUnansweredQuestions.Text = r.QuestionsUnanswered.ToString();
            if (r.IsPassed)
            {
                lblStatus.Text = "Положен";
                lblStatus.ForeColor = Color.Green;
            }
            else
            {
                lblStatus.Text = "Неположен";
                lblStatus.ForeColor = Color.Red;
            }
        }
    }
}
